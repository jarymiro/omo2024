package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(callSuper = true)
public class Customer extends Person {
    protected final String customerCardNo;

    public Customer(String forename, String surname, Address address, Date creationDate, String customerCardNo) {
        super(forename, surname, address, creationDate);
        this.customerCardNo = customerCardNo;
    }

    public static Customer generateRandom() {
        return new Customer(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS),
                faker.idNumber().valid()
        );
    }
}
