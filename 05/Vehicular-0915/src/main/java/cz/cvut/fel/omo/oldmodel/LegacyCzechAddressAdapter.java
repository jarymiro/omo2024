package cz.cvut.fel.omo.oldmodel;

import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Country;

public class LegacyCzechAddressAdapter extends Address {
    private LegacyCzechAddress address;

    public LegacyCzechAddressAdapter(LegacyCzechAddress address) {
        super(address.getMesto(),
                address.getUlice(),
                String.valueOf(address.getCisloPopisne()),
                Country.CZE,
                address.getPsc()
        );
        this.address = address;
    }

    @Override
    public String getHouseNo() {
        return String.format("%d/%d", address.getCisloPopisne(), address.getCisloOrientacni());
    }

    @Override
    public void setCity(String city) {
        address.setMesto(city);
    }
}
