package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@AllArgsConstructor


@Getter
@Setter
@ToString
public class Vehicle {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private final VehicleType type;
    private String serialNo;
    private String registrationNo;
    private int seatCount;
    private Date dateOfManufacturing;
    private Date dateOfPurchase;
    private boolean isRented;
    private double mileage;
    private double tankCapacity;
    private String maker;
    private String model;
    private Branch branch;

    public static Vehicle generateRandom() {
        return new Vehicle(
                VehicleType.getRandom(),
                faker.idNumber().valid(),
                faker.idNumber().ssnValid(),
                faker.random().nextInt(8),
                faker.date().past(365 * 20, TimeUnit.DAYS),
                faker.date().past(30, TimeUnit.DAYS),
                false,
                faker.random().nextDouble() * 20000,
                faker.random().nextDouble() * 100,
                faker.company().name(),
                faker.company().buzzword(),
                null
        );
    }


    public Rental rentTo(Customer customer) {
        // TODO proper logic
        return null;
    }
}
