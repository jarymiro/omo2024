package cz.cvut.fel.omo;

import cz.cvut.fel.omo.model.*;
import cz.cvut.fel.omo.oldmodel.LegacyCzechAddress;
import cz.cvut.fel.omo.oldmodel.LegacyCzechAddressAdapter;

public class Main {
    public static void main(String[] args) {
        // System.out.println(Address.generateRandom());
        // System.out.println(Customer.generateRandom());
        // System.out.println(Employee.generateRandom());
        // System.out.println(Manager.generateRandom());
        // System.out.println(Vehicle.generateRandom());

        LegacyCzechAddress legacyAddress = new LegacyCzechAddress(
                "Technická",
                "Praha",
                1603,
                2,
                "19000"
        );
        Address adaptedAddress = new LegacyCzechAddressAdapter(legacyAddress);
        System.out.println(adaptedAddress.getHouseNo());
    }
}