package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.Constants;

import java.util.Random;

public enum Country {
    SLOVAKIA("Slovakia", "svk"),
    CZECHIA("Czechia", "cze"),
    ROMANIA("Romania", "rom"),
    POLAND("Poland", "pln");

    private static final Random random = new Random(Constants.RANDOM_SEED);

    private String name;
    private String code;

    Country(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public static Country getRandom() {
        return values()[random.nextInt(values().length)];
    }
}
