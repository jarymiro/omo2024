package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Branch {
    private BigDecimal fuelPrice;
    private Address address;
    private String name;
    private List<Employee> employees;
    private Manager manager;
    private List<Vehicle> vehicles;

    public Branch(BigDecimal fuelPrice, Address address, String name, Manager manager) {
        this(fuelPrice, address, name, new ArrayList<>(), manager, new ArrayList<>());
    }

    /**
     * Employ provided person in this branch
     *
     * @param person
     */
    public void employPerson(Person person) {
        // TODO proper logic
    }

    /**
     * Promote provided employee to a manager of the branch.
     *
     * @param employee
     */
    public void promoteToManager(Employee employee) {
        // TODO proper logic
    }

    /**
     * Add provided vehicle to the branch's fleet
     *
     * @param vehicle
     */
    public void addVehicleToFleet(Vehicle vehicle) {
        if(vehicle.getBranch()==null && !vehicles.contains(vehicle)){
            vehicle.setBranch(this);
            vehicles.add(vehicle);
        }else{
            System.err.println("vehicle in branch already");
        }
    }
}
