package cz.cvut.fel.omo;

import cz.cvut.fel.omo.legacydata.LegacySlovakAddress;
import cz.cvut.fel.omo.legacydata.LegacySlovakAddressAdapter;
import cz.cvut.fel.omo.model.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(Address.generateRandom());
        System.out.println(Customer.generateRandom());
        System.out.println(Employee.generateRandom());
        System.out.println(Manager.generateRandom());
        System.out.println(Vehicle.generateRandom());

        LegacySlovakAddress legacy = new LegacySlovakAddress(
                "Žilina",
                "Trienčianská",
                154,
                21,
                "22541"
        );
        LegacySlovakAddressAdapter adapter = new LegacySlovakAddressAdapter(legacy);

        System.out.println(adapter.toString());
        Customer customer = new Customer(
                "Péter",
                "Langoš",
                adapter,
                "0"
        );

        System.out.println(customer.toString());
    }
}