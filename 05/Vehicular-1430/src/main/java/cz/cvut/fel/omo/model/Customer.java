package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class Customer extends Person {
    protected String driverLicenseNo;

    public Customer(String forename, String surname, Address address, String driverLicenseNo) {
        super(forename, surname, address);
        this.driverLicenseNo = driverLicenseNo;
    }

    public static Customer generateRandom() {
        return new Customer(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.idNumber().valid()
        );
    }
}
