package cz.cvut.fel.omo.legacymodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LegacyCzechAddress {
    private int cisloPopisne;
    private int cisloOrientacni;
    private String mesto;
    private String ulice;
}
