package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.Constants;

import java.util.Random;

public enum Country {
    CZE("Czech Republic", "CZK"),
    SVK("Slovak Republic", "SVK"),
    PLN("Poland", "PLN"),
    AUT("Austria", "AUT"),
    FRA("France", "FRA");

    private static final Random random = new Random(Constants.RANDOM_SEED);

    private final String name;
    private final String code;

    Country(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public static Country getRandom() {
        return values()[random.nextInt(values().length)];
    }
}
