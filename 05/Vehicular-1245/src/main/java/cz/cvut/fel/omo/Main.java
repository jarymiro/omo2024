package cz.cvut.fel.omo;

import cz.cvut.fel.omo.legacymodel.LegacyCzechAddress;
import cz.cvut.fel.omo.legacymodel.LegacyCzechAddressAdapter;
import cz.cvut.fel.omo.legacymodel.LegacyRental;
import cz.cvut.fel.omo.legacymodel.LegacyRentalAdapter;
import cz.cvut.fel.omo.model.*;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        System.out.println(Address.generateRandom());
        System.out.println(Customer.generateRandom());
        System.out.println(Employee.generateRandom());
        System.out.println(Manager.generateRandom());
        System.out.println(Vehicle.generateRandom());

        Date fromDate = Date.from(Instant.now().minusSeconds(3600));
        Date toDate = Date.from(Instant.now().plusSeconds(3600));
        Rental rental = new Rental(null,null,null,null,null, fromDate, toDate);
        System.out.println(rental.isActive());


        LegacyRental legacyRental = new LegacyRental(
                "pepa123",
                "1A5 2225",
                300,
                5
        );
        LegacyRentalAdapter adapted = new LegacyRentalAdapter(legacyRental);
        System.out.println(adapted.getTotalPrice());

        LegacyCzechAddress legacyCzechAddress = new LegacyCzechAddress(1603, 2, "Praha", "Technická");
        LegacyCzechAddressAdapter legacyCzechAddressAdapter = new LegacyCzechAddressAdapter(legacyCzechAddress);
        System.out.println(legacyCzechAddressAdapter.toString());
    }



}