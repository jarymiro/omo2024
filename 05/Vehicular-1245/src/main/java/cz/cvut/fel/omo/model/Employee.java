package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.TimeUnit;


@Getter
@Setter
@ToString(callSuper = true)
public class Employee extends Person {
    private static int employeeCount = 0;
    protected int employeeNo;
    protected Date employmentDate;

    public Employee(String username, String forename, String surname, Address address, Date employmentDate) {
        super(username, forename, surname, address);
        this.employeeNo = ++employeeCount;
        this.employmentDate = employmentDate;
    }

    public static Employee generateRandom() {
        return new Employee(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS)
        );
    }
}
