---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 05

Singleton, Adaptér
& Template Method

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

- první design patterny 🎉
  - Singleton
  - Adaptér
  - Template Method
- diskuze k 2. domácímu úkolu
- pokračování v práci z minula
  - doimplementovaný objektový návrh
  - přidání několika užitečných metod
  - ukázka design patternů viz výše

![center-right width:700](../media/factorio-spaghetti.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## [Singleton](https://refactoring.guru/design-patterns/singleton) - První (anti)pattern

- = pouze jedna instance třídy
  - zamezení zbytečnému vytváření objektů
- může skrývat špatný kód
- špatně se testuje

```java
class Singleton {
    private static Singleton instance = null;

    private Singleton() {...}
    public static synchronized getInstance() {
        if (this.instance == null) {
            this.instance = new Singleton();
        }
        return this.instance;
    }
}
```

![bg right:43% height:380](../media/singleton-meme.webp)

## [Adaptér](https://refactoring.guru/design-patterns/adapter) - Zachránce legacy kódu

- strukturální design pattern
- problém - potřebujeme "adaptovat" třídy
  pro fungování na novém kódu
  - starý kód ale nesmíme změnit - zpětná kompatibilita!
- příklad - mix datových zdrojů
  - máme starou a novou implementaci datových tříd
  - starou třídu "obalíme" a můžeme ji používat i v novém kódu
    (funkcím mohu předávat i potomky daného typu)

![top-right width:560](../media//schema-adapter.webp)

## [Adaptér](https://refactoring.guru/design-patterns/adapter) - příklad

```java
public class Car {
    public double pocetKilometru() {...}
}
public class NewCar {
    public double getMileage() {...}
}
public class CarAdapter extends NewCar {
    private final Car car;
    public String getMileage() { return String.valueof(car.pocetKilometru()); }
}

public void processCar(NewCar car) {
    car.getMileage();
}

NewCar audi = new CarAdapter(new Car());
processCar(audi);
```

![bg right:35% height:330](../media/adapter-meme.webp)

## [Template Method](https://refactoring.guru/design-patterns/template-method) - když se algoritmy podobají

- behaviorální design pattern
- problém - algoritmus, jehož části se mohou lišit
- příklad - AI ve hře
  - ovce se pohne, sežere trávu, zabečí, pohne...
  - slepice se pohne, zakvoká, snese vejce, pohne...
  - zombie se pohne, zabručí, zkusí zaútočit, zabručí, pohne...
- příklad - kreslení grafů
  - program zpracuje vstupní data a nakreslí z nich graf
  - tři úseky - *načtení dat* -> *zpracování* -> *vynesení na osy*

![top-right width:385](../media//schema-template-method.webp)

## [Template Method](https://refactoring.guru/design-patterns/template-method) - příklad s AI ve hře

```java
public abstract class Entity {
    public void move() {}
    public abstract void doAction();

    public final void tick() {
        move();
        doAction();
    }
}
public class Sheep extends Entity {
    public void doAction() {
        // eat grass and bleat
    }
}
public class Zombie extends Entity {
    public void doAction() {
        // grumble, attack and grumble again
    }
}
```

![bg right:30% height:600](../media/minecraft-zombie.gif)

## [Template Method](https://refactoring.guru/design-patterns/template-method) - příklad s grafy

```java
public abstract class GraphProcessor {
    public void readData(InputStream is) {}
    public abstract void processData();
    public String outputSvg() {}
}
public class FootballMatchGraph extends GraphProcessor {
    public void processData() {
        // calc statistics and save them to XY coords
    }
}
public class ProjectileThrowGraph extends GraphProcessor {
    public void processData() {
        // calc velocity based on elapsed time and save it to XY coords
    }
}
```

## Vložka na závěr - [Optional](https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html)

- od Javy 8
- kontejner pro objekty
  - implementován pomocí generik
  - zpětně kompatibilní řešení problémů null-safety
- `get()` pro vrácení hodnoty
  (nebo `NoSuchElementException` pokud `null`)
- `orElse()` pro fallback hodnotu

```java
String invalidName = null;
String validName = "Pepa";

// Zde bude result roven "Pepa"
String result = Optional.ofNullable(validName).orElse("Lojza");
// Zde bude result roven "Lojza"
String result = Optional.ofNullable(invalidName).orElse("Lojza");
```

![bg right:33% height:500](../media/java-null-safety-meme.webp)

## [2. domácí úkol](https://cw.fel.cvut.cz/b241/courses/b6b36omo/hw02) - Oprava cizího kódu

- deadline 03. 11.
- tři části
  - Adaptér
  - Template Method
  - Null-safety
- vše důležité probráno dnes 😉
- příští týden prostor na dotazy

![center-right height:550](../media/hw2-meme.webp)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Implementace základních funkcí

- projít si hotový objektový návrh
- implementovat vypsané metody
- vyzkoušet si použití design patternů
