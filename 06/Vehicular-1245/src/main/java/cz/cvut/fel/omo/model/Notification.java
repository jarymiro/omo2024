package cz.cvut.fel.omo.model;

public record Notification(
        String author,
        String message
) {
}
