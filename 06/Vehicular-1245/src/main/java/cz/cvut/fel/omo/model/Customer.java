package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(callSuper = true)
public class Customer extends Person {
    protected String drivingLicenseNo;

    public Customer(String username, String forename, String surname, Address address, String drivingLicenseNo) {
        super(username, forename, surname, address);
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public static Customer generateRandom() {
        return new Customer(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.idNumber().valid()
        );
    }

    public void receive(Notification notification) {
        System.out.println(String.format("%s recieved notification: %s", this.username, notification.toString()));
    }
}
