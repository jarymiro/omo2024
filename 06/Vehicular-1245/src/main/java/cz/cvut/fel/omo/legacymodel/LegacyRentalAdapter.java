package cz.cvut.fel.omo.legacymodel;

import cz.cvut.fel.omo.model.Rental;

import java.math.BigDecimal;

public class LegacyRentalAdapter extends Rental {
    private LegacyRental legacyRental;

    public LegacyRentalAdapter(LegacyRental legacyRental) {
        super();
        this.legacyRental = legacyRental;
    }

    @Override
    public BigDecimal getTotalPrice() {
        return new BigDecimal(legacyRental.getCenaZaDen() * legacyRental.getPocetDni());
    }
}
