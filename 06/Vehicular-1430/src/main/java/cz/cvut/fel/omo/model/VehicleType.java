package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.Constants;

import java.util.Random;

public enum VehicleType {
    CAR,
    TRUCK,
    VAN,
    BOAT,
    AIRPLANE;

    private static final Random random = new Random(Constants.RANDOM_SEED);

    public static VehicleType getRandom() {
        return values()[random.nextInt(values().length)];
    }
}
