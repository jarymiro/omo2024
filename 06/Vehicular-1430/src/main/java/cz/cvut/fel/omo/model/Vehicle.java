package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Random;

@Getter
@Setter
@ToString(exclude = "branch")
@AllArgsConstructor
public class Vehicle {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private String make;
    private String model;
    private int capacity;
    private double mass;
    private VehicleType type;
    private String callCode;
    private String color;
    private int modelYear;
    private double fuelCapacity;
    private Branch branch;


    public static Vehicle generateRandom() {
        return new Vehicle(
                faker.company().name(),
                faker.company().buzzword(),
                faker.random().nextInt(8),
                1000 + faker.random().nextDouble() * 5000,
                VehicleType.getRandom(),
                faker.idNumber().ssnValid(),
                faker.color().name(),
                faker.random().nextInt(1960, 2024),
                faker.random().nextDouble() * 100,
                null
        );
    }

    public Rental rentTo(Customer customer) {
        // TODO proper logic
        return null;
    }

}
