package cz.cvut.fel.omo.factory;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import cz.cvut.fel.omo.model.VehicleType;
import lombok.AllArgsConstructor;

import java.util.Date;

@AllArgsConstructor
public class VehicleFactory {
    private String make;

    public Vehicle makeUsedCar(String model, int capacity, double mass, String callCode, String color, int modelYear, double fuelCapacity, Branch branch) {
        return new Vehicle(
                this.make,
                model,
                capacity,
                mass,
                VehicleType.CAR,
                callCode,
                color,
                modelYear,
                fuelCapacity,
                branch
        );
    }

    public Vehicle makeNewCar(String model, int capacity, double mass, String callCode, String color, double fuelCapacity, Branch branch) {
        return new Vehicle(
                this.make,
                model,
                capacity,
                mass,
                VehicleType.CAR,
                callCode,
                color,
                (new Date()).getYear(),
                fuelCapacity,
                branch
        );
    }

    public Vehicle makeNewBrandedCar(String model, int capacity, double mass, String callCode, double fuelCapacity, Branch branch) {
        Vehicle vehicle = new Vehicle(
                this.make,
                model,
                capacity,
                mass,
                VehicleType.CAR,
                callCode,
                "purple",
                (new Date()).getYear(),
                fuelCapacity,
                branch
        );

        System.out.println(String.format("New branded car: %s",vehicle));
        return vehicle;
    }


}
