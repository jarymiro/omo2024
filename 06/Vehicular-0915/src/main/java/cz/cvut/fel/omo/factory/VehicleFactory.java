package cz.cvut.fel.omo.factory;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import cz.cvut.fel.omo.model.VehicleType;
import lombok.AllArgsConstructor;

import java.time.LocalDate;
import java.util.Date;

@AllArgsConstructor
public class VehicleFactory {
    private Branch branch;

    public Vehicle createNewCar(String serialNo, String registrationNo, int seatCount, double tankCapacity, String maker, String model) {
        Vehicle vehicle = new Vehicle(VehicleType.CAR,
                serialNo,
                registrationNo,
                seatCount,
                new Date(),
                new Date(),
                false,
                0,
                tankCapacity,
                maker,
                model,
                this.branch
        );
        System.err.println(String.format("Created new vehicle %s", vehicle.toString()));
        return vehicle;
    }

    public Vehicle createNewBoeingPlane(String serialNO, String registrationNO, int seatCount, double tankCapacity, String model) {
        Vehicle vehicle = new Vehicle(VehicleType.PLANE,
                serialNO,
                registrationNO,
                seatCount,
                new Date(),
                new Date(),
                false,
                0,
                tankCapacity,
                "Boeing",
                model,
                this.branch
                );
        System.err.println(String.format("Created new boeing plane %s", vehicle.toString()));
        return vehicle;
    }
}
