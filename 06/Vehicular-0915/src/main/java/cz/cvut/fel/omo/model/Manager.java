package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(callSuper = true)
public class Manager extends Employee {
    protected final Date dateOfPromotion;

    public Manager(String forename, String surname, Address address, Date creationDate, Date dateOfEmployment, Date dateOfPromotion) {
        super(forename, surname, address, creationDate, dateOfEmployment);
        this.dateOfPromotion = dateOfPromotion;
    }

    public static Manager generateRandom() {
        final Date creationDate = faker.date().past(365, TimeUnit.DAYS);
        return new Manager(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                creationDate,
                creationDate,
                faker.date().past(30, TimeUnit.DAYS)
        );
    }
}
