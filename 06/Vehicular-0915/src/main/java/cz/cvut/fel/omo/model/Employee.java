package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(callSuper = true)
public class Employee extends Person {
    protected final Date dateOfEmployment;

    // TODO method for creating Employee from Manager?

    public Employee(String forename, String surname, Address address, Date creationDate, Date dateOfEmployment) {
        super(forename, surname, address, creationDate);
        this.dateOfEmployment = dateOfEmployment;
    }

    public static Employee generateRandom() {
        final Date creationDate = faker.date().past(365, TimeUnit.DAYS);
        return new Employee(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                creationDate,
                creationDate
        );
    }
}
