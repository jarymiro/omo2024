package cz.cvut.fel.omo;

import cz.cvut.fel.omo.factory.VehicleFactory;
import cz.cvut.fel.omo.model.*;
import cz.cvut.fel.omo.oldmodel.LegacyCzechAddress;
import cz.cvut.fel.omo.oldmodel.LegacyCzechAddressAdapter;

import java.math.BigDecimal;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        // System.out.println(Address.generateRandom());
        // System.out.println(Customer.generateRandom());
        // System.out.println(Employee.generateRandom());
        // System.out.println(Manager.generateRandom());
        // System.out.println(Vehicle.generateRandom());

        // LegacyCzechAddress legacyAddress = new LegacyCzechAddress(
        //         "Technická",
        //         "Praha",
        //         1603,
        //         2,
        //         "19000"
        // );
        // Address adaptedAddress = new LegacyCzechAddressAdapter(legacyAddress);
        // System.out.println(adaptedAddress.getHouseNo());

        // Starý způsob bez factory
        Vehicle a = new Vehicle(VehicleType.CAR, "123", "AAA 555", 5, new Date(), new Date(), false, 0, 50, "BMW", "318i", null);

        // S factory
        VehicleFactory factory = new VehicleFactory(null);
        Vehicle b = factory.createNewCar("123", "AAA 555", 5, 50, "BMW", "318i");

        System.out.println(a.toString());
        System.out.println(b.toString());

        Customer customer = Customer.generateRandom();
        Customer customerA = Customer.generateRandom();
        Customer customerB = Customer.generateRandom();
        Customer customerC = Customer.generateRandom();
        Branch branch = new Branch(null, Address.generateRandom(), "Moje Pobočka", new BigDecimal(15));

        branch.subscribe(customer);
        branch.addVehicleToFleet(a);

    }
}