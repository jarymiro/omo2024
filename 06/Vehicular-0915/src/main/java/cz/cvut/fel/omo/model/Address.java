package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Random;

@AllArgsConstructor

@Getter
@Setter
@ToString
public class Address {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private String city;
    private String street;
    private String houseNo;
    private Country country;
    private String postalCode;

    public static Address generateRandom() {
        com.github.javafaker.Address fakeAddress = faker.address();
        return new Address(
                fakeAddress.city(),
                fakeAddress.streetAddress(),
                fakeAddress.buildingNumber(),
                Country.getRandom(),
                fakeAddress.zipCode()
        );
    }
}
