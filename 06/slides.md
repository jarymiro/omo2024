---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 06

Factories, Observer

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

- opakování design patternů
  - Simple Factory
  - Abstract Factory
  - Factory Method
  - Observer
- zapracování patternů do kódu viz výše

![center-right width:600](../media/factory-must-grow.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Factory - Třída na třídy

- máme složité objekty se spoustou vlastností
  - ne všechny je ale nutné explicitně definovat
  - chceme poskytnout jednodušší rozhraní pro vytváření objektů
- řešení = továrny!
- proč nepoužít overloadované konstruktory?
  - čitelnost (metody v továrně mají **jméno**)
  - snazší refaktorizace (výměna třídy za jejího potomka)
  - jasné vymezení co a jak nabízím, snazší logování...
- **Simple Factory**, existují ale i pokročilejší verze...

![bottom-right width:450](../media/factory-for-factories.webp)

## [Factory Method](https://refactoring.guru/design-patterns/factory-method) - "Jednoduchá továrna"

- problém - máme **jednu rodinu** objektů
  - např. dialogová okna pro Windows, MacOS...
  - chceme je rozumně a systematicky vytvářet
- řešení = Factory Method
  - vytvoříme společné rozhraní pro továrny,
    která vrací obecné předky tříd
  - následně vytvoříme továrnu pro každý objekt
- další příklady
  - pila, vyrábějící prkna z různých dřev
  - banka, tisknoucí peníze daného státu

![top-right width:650](../media/uml-factory-method.webp)

## [Factory Method](https://refactoring.guru/design-patterns/factory-method) - "Jednoduchá továrna"

máme třídu `Dialog`, a potomka pro použití ve Windows a MacOS

```java
abstract class DialogFactory {
  // Jak bude vypadat metoda na vytvoření dialogu?
}
```

![bottom-center height:350](../media/factory-method.webp)

## [Factory Method](https://refactoring.guru/design-patterns/factory-method) - "Jednoduchá továrna"

máme třídu `Dialog`, a potomka pro použití ve Windows a MacOS

```java
abstract class DialogFactory {
  public abstract Dialog createDialog();
}

class WindowsDialogFactory extends DialogFactory {
  // Jak bude vypadat metoda na vytvoření dialogu?
}

class MacDialogFactory extends DialogFactory {
  // Jak bude vypadat metoda na vytvoření dialogu?
}
```

## [Factory Method](https://refactoring.guru/design-patterns/factory-method) - "Jednoduchá továrna"

máme třídu `Dialog`, a potomka pro použití ve Windows a MacOS

```java
abstract class DialogFactory {
  public abstract Dialog createDialog();
}

class WindowsDialogFactory extends DialogFactory {
  public Dialog createDialog() { return new WindowsDialog(); }
}

class MacDialogFactory extends DialogFactory {
  public Dialog createDialog() { return new MacDialog(); }
}
```

## [Abstract Factory](https://refactoring.guru/design-patterns/abstract-factory) - "Komplexní" továrna

- problém - máme **rodiny** objektů, které sdílí "typování"
  - např. tlačítka, checkboxy apod.,
    každé může vypadat retro, moderně, průsvitně...
  - chceme je rozumně a systematicky vytvářet
- řešení = Abstract Factory
  - vytvoříme rozhraní pro všechny továrny,
    která vrací obecné předky tříd
  - následně vytvoříme továrnu pro každý "typ"
- další příklady
  - továrny na řadu aut různých značek
  - výrobci komponentů do počítačů

![top-right width:650](../media/uml-abstract-factory.webp)

## [Abstract Factory](https://refactoring.guru/design-patterns/abstract-factory) - "Komplexní" továrna

máme `Checkbox` a `Button`, a jejich moderní a retro potomky

```java
abstract class AbstractUiFactory {
  // Jak bude vypadat metoda na vytvoření tlačítka?

  // Jak bude vypadat metoda na vytvoření checkboxu?
}
```

![bottom-center height:350](../media/abstract-factory.webp)

## [Abstract Factory](https://refactoring.guru/design-patterns/abstract-factory) - "Komplexní" továrna

máme `Checkbox` a `Button`, a jejich moderní a retro potomky

```java
abstract class AbstractUiFactory {
  public abstract Button createButton();

  public abstract Checkbox createCheckbox();
}
```

![bottom-center height:350](../media/abstract-factory.webp)

## [Abstract Factory](https://refactoring.guru/design-patterns/abstract-factory) - "Komplexní" továrna

```java
abstract class AbstractUiFactory {
  public abstract Button createButton();

  public abstract Checkbox createCheckbox();
}

class RetroUiFactory extends AbstractUiFactory {
  // Jak bude vypadat metoda na vytvoření tlačítka?

  // Jak bude vypadat metoda na vytvoření checkboxu?
}

class ModernUiFactory extends AbstractUiFactory {
  // Jak bude vypadat metoda na vytvoření tlačítka?

  // Jak bude vypadat metoda na vytvoření checkboxu?
}
```

## [Abstract Factory](https://refactoring.guru/design-patterns/abstract-factory) - "Komplexní" továrna

```java
abstract class AbstractUiFactory {
  public abstract Button createButton();

  public abstract Checkbox createCheckbox();
}

class RetroUiFactory extends AbstractUiFactory {
  public Button createButton() { return new RetroButton(); }

  public Checkbox createCheckbox() { return new RetroCheckbox(); }
}

class ModernUiFactory extends AbstractUiFactory {
  public Button createButton() { return new ModernButton(); }

  public Checkbox createCheckbox() { return new ModernCheckbox(); }
}
```

## [Observer](https://refactoring.guru/design-patterns/observer) - Abys zůstal v obraze

- problém - potřebujeme předávat informace
  z jednoho typu objektů do druhého
  - např. upozornění na produkt pro zákazníky
  - každý zákazník chce být informován jen o něčem
- řešení = Observer
  - odesilatel (Publisher) a příjemce (Subscriber)
  - odesilatel má seznam příjemců, které má notifikovat
- další příklady
  - přihazování v aukci
  - distribuované logování různých částí systému

![top-right width:620](../media/uml-observer.webp)

## [Observer](https://refactoring.guru/design-patterns/observer) - Abys zůstal v obraze

```java
class Customer {
  // Jak bude vypadat metoda na přijetí zprávy?
}

class Product {
  private List<Customer> subscribers;
  // Jak bude vypadat metoda na přidání odběratele?
  // Jak bude vypadat metoda na odstranění odběratele?
  
  // Jak bude vypadat metoda na notifikování odběratelů?
}
```

## [Observer](https://refactoring.guru/design-patterns/observer) - Abys zůstal v obraze

```java
class Customer {
  public void notify(Context context) {...}
}

class Product {
  private List<Customer> subscribers;
  public boolean addSubscriber(Customer customer) {...}
  public boolean removeSubscriber(Customer customer) {...}
  
  // Jak bude vypadat metoda na notifikování odběratelů?
}
```

## [Observer](https://refactoring.guru/design-patterns/observer) - Abys zůstal v obraze

```java
class Customer {
  public void notify(Context context) {...}
}

class Product {
  private List<Customer> subscribers;
  public boolean addSubscriber(Customer customer) {...}
  public boolean removeSubscriber(Customer customer) {...}
  
  public void notifySubscribers() {
    for (Customer subscriber : subscribers) {
      subscriber.notify(...);
    }
  }
}
```

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Továrny a pozorovatelé

- vytvořit třídu **VehicleFactory**
  - v balíčku **cz.cvut.fel.omo.factory**
  - dvě metody pro každý typ vozidla (ojeté a nové auto)
- implementovat možnost zákazníka odebírat pobočku
  - když do pobočky přijde nové auto, odběratelé dostanou notifikaci
  - když v pobočce klesne cena paliva, odběratelé dostanou notifikaci
