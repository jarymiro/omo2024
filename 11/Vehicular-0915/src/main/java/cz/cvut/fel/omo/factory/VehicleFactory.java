package cz.cvut.fel.omo.factory;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import cz.cvut.fel.omo.model.VehicleType;
import lombok.AllArgsConstructor;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@AllArgsConstructor
public class VehicleFactory {
    private Branch branch;

    public Vehicle createNewCar(String serialNo, String registrationNo, int seatCount, double tankCapacity, String maker, String model) {
        Vehicle vehicle = new Vehicle(VehicleType.CAR,
                serialNo,
                registrationNo,
                seatCount,
                new Date(),
                new Date(),
                false,
                0,
                tankCapacity,
                maker,
                model,
                this.branch
        );
        System.err.printf("Created new vehicle %s%n", vehicle);
        return vehicle;
    }

    public Vehicle createNewBoeingPlane(String serialNO, String registrationNO, int seatCount, double tankCapacity, String model) {
        Vehicle vehicle = new Vehicle(VehicleType.PLANE,
                serialNO,
                registrationNO,
                seatCount,
                new Date(),
                new Date(),
                false,
                0,
                tankCapacity,
                "Boeing",
                model,
                this.branch
        );
        System.err.printf("Created new boeing plane %s%n", vehicle);
        return vehicle;
    }

    public Vehicle createNewFrontierBoat(String serialNo, String registrationNo, int seatCount, double tankCapacity, String model) {
        Vehicle b = new Vehicle(
                VehicleType.BOAT,
                serialNo,
                registrationNo,
                seatCount,
                new Date(),
                new Date(),
                false,
                0,
                tankCapacity,
                "Frontier",
                model,
                this.branch
        );
        System.err.printf("Created new Frontier boat: %s%n", b);
        return b;
    }

    public Vehicle createFakeVehicle() {
        Faker faker = new Faker();
        Vehicle fakeVehicle = new Vehicle(
                VehicleType.getRandom(),
                faker.idNumber().valid(),
                faker.idNumber().ssnValid(),
                faker.random().nextInt(8),
                faker.date().past(365 * 20, TimeUnit.DAYS),
                faker.date().past(30, TimeUnit.DAYS),
                false,
                faker.random().nextDouble() * 20000,
                faker.random().nextDouble() * 100,
                faker.company().name(),
                faker.company().buzzword(),
                null
        );
        System.err.printf("Fake Vehicle was created: %s%n", fakeVehicle);
        return fakeVehicle;
    }

}
