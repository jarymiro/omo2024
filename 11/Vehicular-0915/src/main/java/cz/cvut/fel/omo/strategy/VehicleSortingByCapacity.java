package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.List;

public class VehicleSortingByCapacity extends VehicleSorting {
    public VehicleSortingByCapacity(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = branch.getVehicles()
                .stream()
                .sorted((a, b) -> {
                    return Integer.compare(a.getSeatCount(), b.getSeatCount());
                    // return a.getSeatCount() - b.getSeatCount();
                })
                .toList();

        return vehicles;
    }
}
