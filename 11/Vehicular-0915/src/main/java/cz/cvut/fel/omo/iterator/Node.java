package cz.cvut.fel.omo.iterator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Node<E> {
    protected Node<E> next;
    protected Node<E> prev;
    protected E value;

    public Node(E value) {
        this.next = null;
        this.prev = null;
        this.value = value;
    }

    public void setNext(Node<E> next) {
        this.next = next;
        next.setPrev(this);
    }
}
