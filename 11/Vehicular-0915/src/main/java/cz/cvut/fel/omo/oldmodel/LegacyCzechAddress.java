package cz.cvut.fel.omo.oldmodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class LegacyCzechAddress {
    private String ulice;
    private String mesto;
    private int cisloPopisne;
    private int cisloOrientacni;
    private String psc;
}
