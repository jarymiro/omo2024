package cz.cvut.fel.omo.iterator;

import cz.cvut.fel.omo.model.Vehicle;

public interface Iterator<E extends Vehicle> {
    boolean hasNext();

    E getNext();

    void add(E element);
}
