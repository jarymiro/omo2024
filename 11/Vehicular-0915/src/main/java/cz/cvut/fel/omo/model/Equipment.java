package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import cz.cvut.fel.omo.decorator.RentalAdditionDecorator;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


@Getter
@Setter
@ToString
public class Equipment extends RentalAdditionDecorator {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private String name;
    private BigDecimal dailyPrice;
    private List<VehicleType> rentableWith;

    public Equipment(RentalFirstLayer innerLayer, String name, BigDecimal dailyPrice, List<VehicleType> rentableWith) {
        super(innerLayer);
        this.name = name;
        this.dailyPrice = dailyPrice;
        this.rentableWith = rentableWith;
    }

    public boolean isRentableWith(VehicleType vehicleType) {
        return this.rentableWith.contains(vehicleType);
    }

    public Equipment generateRandom(RentalFirstLayer innerLayer) {
        return new Equipment(
                innerLayer,
                faker.commerce().productName(),
                BigDecimal.valueOf(faker.random().nextDouble() * 30000),
                Arrays.asList(VehicleType.values())
        );
    }

    @Override
    public BigDecimal getPrice() {
        BigDecimal totalPrice = getDailyPrice().multiply(new BigDecimal(innerLayer.getRentalDays()));
        return innerLayer.getPrice().add(totalPrice);
    }
}
