package cz.cvut.fel.omo.configuration;

import cz.cvut.fel.omo.decorator.RentalInsuranceFee;
import cz.cvut.fel.omo.model.*;
import cz.cvut.fel.omo.strategy.VehicleSorting;
import cz.cvut.fel.omo.strategy.VehicleSortingByCapacity;
import cz.cvut.fel.omo.strategy.VehicleSortingByPrice;
import cz.cvut.fel.omo.visitor.DataAnalyzer;
import cz.cvut.fel.omo.visitor.InternetDataAnalyzer;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * TADA Template Method
 */
public abstract class Configuration {
    protected final List<Branch> branchList;
    protected final List<Customer> customers;

    public Configuration() {
        this.branchList = new LinkedList<>();
        this.customers = new LinkedList<>();
    }

    public void run() {
        initalize();
        for (int i = 0; i < 200; i++) {
            doTick();
        }
        makeReports();
    }

    protected abstract void initalize();

    protected void doTick() {
        Random random = new Random(500);
        for (Branch branch : branchList) {
            for (Vehicle vehicle : branch.getVehicles()) {
                for (Customer customer : customers) {
                    if (random.nextInt(100) > 90) {
                        // Date today = new Date();
                        // Date tommorow =
                        RentalSecondLayer rental = new RentalInsuranceFee(
                                new Rental(customer, vehicle, branch, vehicle.getDailyPrice(), new Date(), new Date())
                        );
                        System.out.printf("Customer %s rented car %s\n", customer, vehicle);
                    }
                }
            }
        }
    }

    protected void makeReports() {
        System.err.println("Final state of simulation");
        DataAnalyzer analyzer = new InternetDataAnalyzer();
        for (Branch branch : branchList) {
            VehicleSorting sorting = new VehicleSortingByPrice(branch);
            System.out.println(sorting.getSorted());
            sorting = new VehicleSortingByCapacity(branch);
            System.out.println(sorting.getSorted());

            for (Employee employee : branch.getEmployees()) {
                employee.accept(analyzer);
            }
            branch.getManager().accept(analyzer);
        }
    }

}
