package cz.cvut.fel.omo;

import cz.cvut.fel.omo.configuration.BasicConfiguration;
import cz.cvut.fel.omo.configuration.Configuration;
import cz.cvut.fel.omo.iterator.Iterator;
import cz.cvut.fel.omo.iterator.LinkedListIterator;


public class Main {
    public static void main(String[] args) {
        // Configuration configuration = new BasicConfiguration();
        // configuration.run();

        Iterator<Integer> iterator = new LinkedListIterator<>();
        iterator.add(1);
        iterator.add(2);
        iterator.add(3);
        iterator.add(4);

        while (iterator.hasNext()) {
            System.out.println(iterator.getNext());
        }
    }
}