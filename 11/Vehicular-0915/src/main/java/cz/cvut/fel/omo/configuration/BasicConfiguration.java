package cz.cvut.fel.omo.configuration;

import cz.cvut.fel.omo.factory.VehicleFactory;
import cz.cvut.fel.omo.model.*;

import java.math.BigDecimal;
import java.util.Date;

public class BasicConfiguration extends Configuration {
    @Override
    protected void initalize() {
        // Starý způsob bez factory
        Vehicle a = new Vehicle(VehicleType.CAR, "123", "AAA 555", 5, new Date(), new Date(), false, 0, 50, "BMW", "318i", null);

        // S factory
        VehicleFactory factory = new VehicleFactory(null);
        Vehicle b = factory.createNewCar("123", "AAA 555", 5, 50, "BMW", "318i");

        Manager manager = Manager.generateRandom();
        Branch branch = new Branch(manager, Address.generateRandom(), "Moje Pobočka", new BigDecimal(15));
        branchList.add(branch);

        Customer customer = Customer.generateRandom();
        customers.add(customer);
        customers.add(Customer.generateRandom());
        customers.add(Customer.generateRandom());
        customers.add(Customer.generateRandom());
        customers.add(Customer.generateRandom());

        branch.subscribe(customer);
        branch.addVehicleToFleet(a);
        branch.addVehicleToFleet(b);
        branch.addVehicleToFleet(Vehicle.generateRandom());
        branch.addVehicleToFleet(Vehicle.generateRandom());
        branch.addVehicleToFleet(Vehicle.generateRandom());
        branch.addVehicleToFleet(Vehicle.generateRandom());
    }
}
