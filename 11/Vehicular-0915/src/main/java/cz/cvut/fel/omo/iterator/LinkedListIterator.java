package cz.cvut.fel.omo.iterator;

import cz.cvut.fel.omo.model.Vehicle;

public class LinkedListIterator<E extends Vehicle> implements Iterator<E> {
    protected Node<E> first;
    protected Node<E> last;
    protected Node<E> current;

    public LinkedListIterator() {
        this.first = null;
        this.last = null;
        this.current = null;
    }

    public void add(E element) {
        Node<E> node = new Node<>(element);
        if (first == null) {
            // máme prázdný iterátor
            first = node;
            last = node;
            current = node;
        } else {
            last.setNext(node);
            last = node;
        }
    }

    @Override
    public boolean hasNext() {
        if (current != null) {
            return true;
        }
        return false;
        // return current.getNext() != null;
    }

    @Override
    public E getNext() {
        Node<E> original = current;
        current = current.getNext();
        return original.getValue();
    }
}
