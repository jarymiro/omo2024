---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 11

Generické programování

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

* Java Generics ![center-right width:650](../media/end-is-nigh.webp)
  - motivace
  - zápis v kódu
  - horní a dolní omezení
* práce na společné codebase (naposled 😭)
  - průchod pomergovaným kódem
  - přidání generik

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Organizace konce semestru

* [odkaz na rezervavci konzultaci a cvičení](https://calendar.google.com/calendar/appointments/AcZssZ34SG1byLAHcxS801_MuN4U1pa_rjgn3HKO4tY=)
* [odkaz na vyplnění URL repa SP](https://docs.google.com/forms/d/e/1FAIpQLSecBw_qvX8PZb91hB9G4FnrugM2TF3KAc_4N6wo3W3KRFcyjA/viewform?usp=sf_link)
* odevzdání SP do konce 14. týdne (10. 1.)
  - výjimky pouze po dohodě dostatečně předem
* příští týden (17. 12.) cvičení na REST
  a mikroslužby
  - docházka nepovinná
* zápočtový týden (7. 1.) cvičení odpadá ![center-right width:675](../media/odevzdani-sp-meme.webp)
  - místo něj odevzdání SP

## Generika (Java Generics)
- možnost parametrizovat třídy
  - již od Javy 5 (2004)!
  - při vytváření předám jinou třídu jako "parametr"
  - nelze použít na primitivní typy!
- použití
  - Java kolekce
  - bezpečnější "castování"
  - matematické operace (objekt pro vektory - integer/double)
![top-right width:610](../media/java-generics-meme.webp)

## Zápis generik v kódu
![bg right:31% width:97%](../media/generics-n-meme.webp)
- definujeme v hlavičce třídy pomocí zástupného znaku
  - existují [jmenné konvence](https://docs.oracle.com/javase/tutorial/java/generics/types.html) na zástupné znaky
```java
public class Vector2D<N> {
    // místo konkrétní třídy používám zadefinovaný "placeholder"
    protected N x, y;
    // u hlavičky konstruktoru nepíšeme <>
    public Vector2D(N x, N y) {...}
    // Návratový typ bude opět zástupný znak
    public N getX() { return this.x }
    public N getY() { return this.y }

}
// Parametr třídy dávejte pouze k typu proměnné;
// ke konstruktoru ho dodá Java při kompilaci.
Vector2D<Integer> integerVector = new 2DVector<>(2, 3);
Vector2D<Double> doubleVector = new 2DVector<>(5.8, 2.5);
integerVector.getX(); // Vrátí datový typ Integer o hodnotě 2
```

## Zápis generik v kódu - po kompilaci
![bg right:31% width:97%](../media/generics-n-meme-correct.webp)
- příklad `Vector2D<Integer>` z předchozího slidu
- kompilátor bezpečně "doplní" přetypování
```java
public class Vector2D {
    
    protected Object x, y;
    
    public Vector2D(Object x, Object y) {...}
    
    public Object getX() { return this.x }
    public Object getY() { return this.y }
}

Vector2D integerVector = new 2DVector(2, 3);
Vector2D doubleVector = new 2DVector(5.8, 2.5);
(Integer) integerVector.getX(); // Vrátí datový typ Integer o hodnotě 2
```

## Omezení typu generické třídy 
- jak zajistit, abychom neměli vektor stringů (či jiných nečísel)?
  - v hlavičce zajistíme omezení pomocí
    `extends` (horní mez)
    `&` (logický operátor)
  - daná třída pak musí dědičnost splnit (přímo i nepřímo)
  - kontroluje se při kompilaci

```java
public class Integer extends Number {} // viz JavaDoc třídy java.lang.Integer
public class PositiveInteger extends Integer {}

public class Vector2D<N extends Number> { ... }

Vector2D<Integer> = new Vector2D<>();         // OK, třída dědí Number přímo
Vector2D<PositiveInteger> = new Vector2D<>(); // OK, třída dědí Number přes Integer
Vector2D<String> = new Vector2D<>();          // Compile ERROR
```


## Omezení typu reference na generickou třídu
- jak zajistit, abychom přijali pouze určená generika
  - v definici reference zajistíme omezení pomocí
    `extends` (horní mez)
    `super` (dolní mez)
  - daná třída pak musí dědičnost splnit (přímo i nepřímo)
  - kontroluje se při kompilaci

```java
public class Integer extends Number {} // viz JavaDoc třídy java.lang.Integer
public class PositiveInteger extends Integer {}

// OK, List integerů (nebo něčeho víc)
public void printArray(List<? extends Integer> list) { ... }
// OK, List PositiveIntegerů (nebo něčeho míň)
public void printOtherArray(List<? super PositiveInteger> list) { ... }
// Compile ERROR - již existuje jedna metoda s jedním parametrem List (generika zde nemají vliv)
public void printArray(List<? super PositiveInteger> list) { ... }
```

## Další příklady generik

![bg right:36% width:98%](../media/excuse-me.webp)
- generické třídy lze samozřejmě libovolně dědit

```java
public interface List<E> {...}
public interface Map<K, V> {...}
public interface Container<N> {...}

// Implementace spojového seznamu - předám generický typ
public class LinkedList<E> implements List<E> {...}

// Seznam obsahující pouze čísla - omezím generický typ
public class NumberList<N extends Number> implements List<N> {...}

// Negenerická třída implementující generickou třídu
public class StringContainer implements Container<String> {...}

// Generická třída s jedním parametrem
// implementující generickou třídu se dvěma parametry
public class StupidArray<E> implements Map<Integer, E> {...}
```

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Recap & Java Generics

* projděte si zamergované funkcionality
* naimplementujte **Iterator<E>**
  - vyzkoušejme si různá omezení
  - zároveň by mělo jít o design pattern [Iterator](https://refactoring.guru/design-patterns/iterator)