package cz.cvut.fel.omo.legacydata;

import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Country;

public class LegacySlovakAddressAdapter extends Address {
    private final LegacySlovakAddress address;

    public LegacySlovakAddressAdapter(LegacySlovakAddress address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return String.format(
                "Address(city=%s, street=%s, houseNo=%d/%d, country=%s, zip=%s)",
                address.getMesto(),
                address.getUlicia(),
                address.getCiesloPopisne(),
                address.getCiesloOrientacne(),
                Country.SLOVAKIA,
                address.getCiesloSmerovacie()
        );
    }
}
