package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalFirstInterface;

import java.math.BigDecimal;

public class RentalFuelTankFee extends RentalFirstDecorator {

    public RentalFuelTankFee(RentalFirstInterface wrappedRental) {
        super(wrappedRental);
    }

    @Override
    public BigDecimal calcTotalPrice() {
        return wrappedRental.calcTotalPrice().add(new BigDecimal(100));
    }
}
