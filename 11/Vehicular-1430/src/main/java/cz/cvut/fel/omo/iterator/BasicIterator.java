package cz.cvut.fel.omo.iterator;

import cz.cvut.fel.omo.model.Vehicle;

import java.util.List;

public class BasicIterator<T extends Vehicle> implements Iterator<T> {

    protected final List<T> data;
    protected int position;

    public BasicIterator(List<T> data) {
        this.data = data;
        this.position = 0;
    }


    @Override
    public boolean hasMore() {
        return data.size() != position;
    }

    @Override
    public T getCurrentAndIncrement() {
        return data.get(position++);
    }
}
