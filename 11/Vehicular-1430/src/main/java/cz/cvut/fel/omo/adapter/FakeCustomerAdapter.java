package cz.cvut.fel.omo.adapter;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Customer;

public class FakeCustomerAdapter extends Customer {
    private static final Faker faker = new Faker();

    public FakeCustomerAdapter() {
        super(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.idNumber().valid()
        );
    }
}
