package cz.cvut.fel.omo.iterator;

import cz.cvut.fel.omo.model.Vehicle;

public interface Iterator<T extends Vehicle> {
    boolean hasMore();

    T getCurrentAndIncrement();
}
