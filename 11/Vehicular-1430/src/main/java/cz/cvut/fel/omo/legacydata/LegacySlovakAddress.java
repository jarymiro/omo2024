package cz.cvut.fel.omo.legacydata;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LegacySlovakAddress {
    private String mesto;
    private String ulicia;
    private int ciesloPopisne;
    private int ciesloOrientacne;
    private String ciesloSmerovacie;
}
