package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.visitor.DataAnalyzer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(callSuper = true)
public class Manager extends Employee {
    protected final Date promotionDate;

    // TODO method for creating Manager from Employee?

    public Manager(String forename, String surname, Address address, Date empolymentDate, Date promotionDate) {
        super(forename, surname, address, empolymentDate);
        this.promotionDate = promotionDate;
    }

    public static Manager generateRandom() {
        return new Manager(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS),
                faker.date().past(30, TimeUnit.DAYS)
        );
    }

    @Override
    public void accept(DataAnalyzer analyzer) {
        analyzer.processBasic(this);
    }
}
