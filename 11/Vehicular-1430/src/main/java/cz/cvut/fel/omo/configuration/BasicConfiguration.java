package cz.cvut.fel.omo.configuration;

import cz.cvut.fel.omo.factory.VehicleFactory;
import cz.cvut.fel.omo.legacydata.LegacySlovakAddress;
import cz.cvut.fel.omo.legacydata.LegacySlovakAddressAdapter;
import cz.cvut.fel.omo.model.*;

import java.math.BigDecimal;
import java.util.Date;

public class BasicConfiguration extends Configuration {
    @Override
    protected void initalize() {
        System.out.println(Address.generateRandom());
        System.out.println(Customer.generateRandom());
        System.out.println(Employee.generateRandom());
        System.out.println(Manager.generateRandom());
        System.out.println(Vehicle.generateRandom());

        LegacySlovakAddress legacy = new LegacySlovakAddress(
                "Žilina",
                "Trienčianská",
                154,
                21,
                "22541"
        );
        LegacySlovakAddressAdapter adapter = new LegacySlovakAddressAdapter(legacy);

        System.out.println(adapter);
        Customer customer = new Customer(
                "Péter",
                "Langoš",
                adapter,
                "0"
        );

        System.out.println(customer);

        Vehicle a = new Vehicle("Mazda", "Miata", 2, 1050, VehicleType.CAR, "4A1 5555", "Red", 2024, 30, null);
        VehicleFactory mazdaFactory = new VehicleFactory("Mazda");
        Vehicle b = mazdaFactory.makeNewBrandedCar("Miata", 2, 1050, "5A7 9999", 30, null);

        Branch pragueBranch = new Branch(new BigDecimal(35), adapter, "Slovenská ambasáda v Praze", null);
        Customer lada = Customer.generateRandom();
        Customer pepa = Customer.generateRandom();

        pragueBranch.addSubscriber(lada);
        pragueBranch.addVehicleToFleet(a);
        pragueBranch.removeSubscriber(lada);
        pragueBranch.addSubscriber(pepa);
        pragueBranch.addVehicleToFleet(b);

        pragueBranch.addVehicleToFleet(Vehicle.generateRandom());
        pragueBranch.addVehicleToFleet(mazdaFactory.createFakeVehicle());
        pragueBranch.addVehicleToFleet(mazdaFactory.createFakeVehicle());
        pragueBranch.setManager(new Manager("pepa", "neco", Address.generateRandom(), new Date(), new Date()));

        branchList.add(pragueBranch);
        customers.add(customer);
        customers.add(lada);
        customers.add(pepa);
    }
}
