package cz.cvut.fel.omo;

import cz.cvut.fel.omo.configuration.BasicConfiguration;
import cz.cvut.fel.omo.configuration.Configuration;
import cz.cvut.fel.omo.decorator.RentalFuelTankFee;
import cz.cvut.fel.omo.decorator.RentalPersonalDriverFee;
import cz.cvut.fel.omo.factory.VehicleFactory;
import cz.cvut.fel.omo.iterator.BasicIterator;
import cz.cvut.fel.omo.iterator.InfiniteIterator;
import cz.cvut.fel.omo.iterator.Iterator;
import cz.cvut.fel.omo.legacydata.LegacySlovakAddress;
import cz.cvut.fel.omo.legacydata.LegacySlovakAddressAdapter;
import cz.cvut.fel.omo.model.*;
import cz.cvut.fel.omo.strategy.VehicleSorting;
import cz.cvut.fel.omo.strategy.VehicleSortingByCapacity;
import cz.cvut.fel.omo.strategy.VehicleSortingByPrice;
import cz.cvut.fel.omo.visitor.DataAnalyzer;
import cz.cvut.fel.omo.visitor.InternetDataAnalyzer;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new BasicConfiguration();
        configuration.run();

        List<Vehicle> data = new LinkedList<>();
        VehicleFactory factory = new VehicleFactory("Ford");
        data.add(factory.createFakeVehicle());
        data.add(factory.createFakeVehicle());
        data.add(factory.createFakeVehicle());
        data.add(factory.createFakeVehicle());
        data.add(factory.createFakeVehicle());

        Iterator<Vehicle> iterator = new InfiniteIterator<>(data);
        while (iterator.hasMore()) {
            System.out.println(iterator.getCurrentAndIncrement());
        }
    }
}


