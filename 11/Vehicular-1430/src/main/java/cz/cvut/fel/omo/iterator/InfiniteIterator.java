package cz.cvut.fel.omo.iterator;

import cz.cvut.fel.omo.model.Vehicle;

import java.util.List;

public class InfiniteIterator<T extends Vehicle> implements Iterator<T> {

    protected final List<T> data;
    protected int position;

    public InfiniteIterator(List<T> data) {
        this.data = data;
        this.position = 0;
    }


    @Override
    public boolean hasMore() {
        return !data.isEmpty();
    }

    @Override
    public T getCurrentAndIncrement() {
        if (position >= data.size()) {
            position = 0;
        }

        return data.get(position++);
    }
}
