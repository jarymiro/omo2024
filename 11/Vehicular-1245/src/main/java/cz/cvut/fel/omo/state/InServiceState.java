package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;
import org.apache.commons.lang3.NotImplementedException;

import java.util.Date;
import java.util.List;

public class InServiceState extends VehicleState {

    public InServiceState(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public Rental rentTo(Customer customer, Date rentedFrom, Date rentedTo) {
        throw new NotImplementedException("Implement in your free time");
    }

    @Override
    public void unrent(Rental rental) {
        throw new NotImplementedException("Implement in your free time");
    }

    @Override
    public void fix() {
        throw new NotImplementedException("Implement in your free time");
    }

    @Override
    public void wash() {
        throw new NotImplementedException("Implement in your free time");
    }
}
