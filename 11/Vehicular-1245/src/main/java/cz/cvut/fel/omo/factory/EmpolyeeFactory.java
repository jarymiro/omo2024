package cz.cvut.fel.omo.factory;

import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Employee;
import cz.cvut.fel.omo.model.Person;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EmpolyeeFactory extends PersonFactory {
    @Override
    public Person createFromData(String username, String forename, String surname, Address address, Date creationDate) {
        return new Employee(username, forename, surname, address, creationDate, new Date());
    }

    @Override
    public Person createFromData(String username, String forename, String surname, Address address) {
        return new Employee(username, forename, surname, address, new Date());
    }

    @Override
    public Person createFromFakeData() {
        return new Employee(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS)
        );
    }
}
