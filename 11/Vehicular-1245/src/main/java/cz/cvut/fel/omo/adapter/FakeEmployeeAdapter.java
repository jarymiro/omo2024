package cz.cvut.fel.omo.adapter;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Employee;

import java.util.concurrent.TimeUnit;


public class FakeEmployeeAdapter extends Employee {
    private static final Faker faker = new Faker();

    public FakeEmployeeAdapter() {
        super(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS)
        );
    }
}
