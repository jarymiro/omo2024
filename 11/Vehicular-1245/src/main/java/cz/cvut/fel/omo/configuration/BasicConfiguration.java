package cz.cvut.fel.omo.configuration;

import cz.cvut.fel.omo.factory.*;
import cz.cvut.fel.omo.legacymodel.LegacyCzechAddress;
import cz.cvut.fel.omo.legacymodel.LegacyCzechAddressAdapter;
import cz.cvut.fel.omo.legacymodel.LegacyRental;
import cz.cvut.fel.omo.legacymodel.LegacyRentalAdapter;
import cz.cvut.fel.omo.model.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BasicConfiguration extends Configuration {
    @Override
    protected void initalize() {

        String customerUsername = "pepa123";
        Map<String, String> drivingLicenseDb = new HashMap<>();
        drivingLicenseDb.put(customerUsername, "123456");

        PersonFactory personFactory = new CustomerFactory(drivingLicenseDb);
        Person pepa = personFactory.createFromData(customerUsername, "Pepa", "Novák", Address.generateRandom());
        Person pepaB = personFactory.createFromFakeData();

        personFactory = new EmpolyeeFactory();
        Person empolyee = personFactory.createFromFakeData();

        personFactory.createFromFakeData();
        personFactory = new ManagerFactory();
        personFactory.createFromFakeData();
        personFactory.createFromFakeData();


        System.out.println(Address.generateRandom());
        // System.out.println(Customer.generateRandom());
        // System.out.println(Employee.generateRandom());
        // System.out.println(Manager.generateRandom());
        // System.out.println(Vehicle.generateRandom());


        LegacyRental legacyRental = new LegacyRental(
                "pepa123",
                "1A5 2225",
                300,
                5
        );
        LegacyRentalAdapter adapted = new LegacyRentalAdapter(legacyRental);
        System.out.println(adapted.getTotalPrice());

        LegacyCzechAddress legacyCzechAddress = new LegacyCzechAddress(1603, 2, "Praha", "Technická");
        LegacyCzechAddressAdapter legacyCzechAddressAdapter = new LegacyCzechAddressAdapter(legacyCzechAddress);
        System.out.println(legacyCzechAddressAdapter);

        Branch newYork = new Branch(legacyCzechAddressAdapter, null, new BigDecimal(15));

        // newYork.addSubscriber(pepa);
        // newYork.addSubscriber(pepaB);

        Vehicle ford = new Vehicle(new Date(), VehicleType.PERSONALCAR, 0, "AAA BBB", "5A1 2255", "Wine", "Ford", "Galaxy", 50, null);
        newYork.addVehicleToFleet(ford);

        // newYork.removeSubscriber(pepaB);
        VehicleFactory factory = new VehicleFactory(null);
        Vehicle truck = factory.makeNewVolvoTruck("DDD 555", "5A3 6547", "Blue", "FH16", 300);
        newYork.addVehicleToFleet(truck);
        newYork.addVehicleToFleet(factory.createFakeVehicle());
        newYork.addVehicleToFleet(factory.createFakeVehicle());
        newYork.addVehicleToFleet(factory.createFakeVehicle());
        newYork.addVehicleToFleet(factory.createFakeVehicle());

        branchList.add(newYork);
        // Pozor na to jestli opravdu můžu castovat!!!
        customers.add((Customer) pepa);
        customers.add((Customer) pepaB);
        personFactory = new ManagerFactory();
        newYork.setManager((Manager) personFactory.createFromFakeData());
    }
}
