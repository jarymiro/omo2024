package cz.cvut.fel.omo.visitor;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Employee;
import cz.cvut.fel.omo.model.Vehicle;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class DataAnalyzer {
    public void processBasic(Employee employee) {
        System.out.printf("Visited empolyee %s %s\n", employee.getForename(), employee.getSurname());
    }

    public void processAdvanced(Employee employee) {
        System.out.printf("Analyzing performance of empolyee %s %s\n", employee.getForename(), employee.getSurname());
        long dayCount = ChronoUnit.DAYS.between(employee.getEmploymentDate().toInstant(), Instant.now());
        System.out.printf("Employeed for %d days\n", dayCount);
    }


    public void processBasic(Vehicle vehicle) {

    }

    public void processAdvanced(Vehicle vehicle) {

    }


    public void processBasic(Customer customer) {

    }

    public void processAdvanced(Customer customer) {

    }
}
