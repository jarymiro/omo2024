package cz.cvut.fel.omo.factory;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Person;

import java.util.Date;
import java.util.Random;

public abstract class PersonFactory {
    protected static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    public abstract Person createFromData(String username, String forename, String surname, Address address, Date creationDate);

    public abstract Person createFromData(String username, String forename, String surname, Address address);

    public abstract Person createFromFakeData();


}
