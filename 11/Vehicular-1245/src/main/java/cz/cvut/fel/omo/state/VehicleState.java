package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.Date;
import java.util.List;

public abstract class VehicleState {
    protected VehicleState nextState;
    protected Vehicle vehicle;

    public VehicleState(Vehicle vehicle) {
        this.nextState = null;
        this.vehicle = vehicle;
    }

    public abstract Rental rentTo(Customer customer, Date rentedFrom, Date rentedTo);

    public abstract void unrent(Rental rental);

    public abstract void fix();

    public abstract void wash();

    public VehicleState getNextState() {
        if (this.nextState == null) {
            return this;
        } else {
            return nextState;
        }
    }
}
