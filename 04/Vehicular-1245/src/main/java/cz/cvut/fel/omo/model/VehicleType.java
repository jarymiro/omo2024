package cz.cvut.fel.omo.model;

public enum VehicleType {
    PERSONALCAR(), VAN(), TRUCK(), BOAT(), PLANE();
}
