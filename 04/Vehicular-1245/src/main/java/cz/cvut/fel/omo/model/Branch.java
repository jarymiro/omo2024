package cz.cvut.fel.omo.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public class Branch {
    private Address address;
    private Set<Vehicle> vehicles;
    private List<Employee> employees;
    private Manager manager;
    private BigDecimal price;
}
