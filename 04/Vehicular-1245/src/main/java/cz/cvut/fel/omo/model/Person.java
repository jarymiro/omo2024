package cz.cvut.fel.omo.model;

import java.util.Date;

public abstract class Person {
    protected final int id;
    protected String username;
    protected String forename;
    protected String surname;
    protected Address address;
    protected final Date creationDate;

    public Person(int id, String username, String forename, String surname, Address address) {
        this(id, username, forename, surname, address, new Date());
    }

    public Person(int id, String username, String forename, String surname, Address address, Date creationDate) {
        this.id = id;
        this.username = username;
        this.forename = forename;
        this.surname = surname;
        this.address = address;
        this.creationDate = creationDate;

    }
}
