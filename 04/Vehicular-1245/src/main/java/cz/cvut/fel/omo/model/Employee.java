package cz.cvut.fel.omo.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
public class Employee extends Person {
    protected int employeeNo;
    protected Date employmentDate;

    public Employee(int id, String username, String forename, String surname, Address address, int employeeNo, Date employmentDate) {
        super(id, username, forename, surname, address);
        this.employeeNo = employeeNo;
        this.employmentDate = employmentDate;
    }
}
