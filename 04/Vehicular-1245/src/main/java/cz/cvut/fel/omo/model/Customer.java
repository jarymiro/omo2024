package cz.cvut.fel.omo.model;

import lombok.Getter;

@Getter
public class Customer extends Person {
    protected String drivingLicenseNo;

    public Customer(int id, String username, String forename, String surname, Address address, String drivingLicenseNo) {
        super(id, username, forename, surname, address);
        this.drivingLicenseNo = drivingLicenseNo;
    }
}
