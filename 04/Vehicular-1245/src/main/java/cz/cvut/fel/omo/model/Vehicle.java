package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class Vehicle {
    private VehicleType vehicleType;
    private float mileage;
    private final Date productionDate;
    private String vinCode;
    private String spz;
    private String color;
    private String maker;
    private String model;
    private float tankCapacity;
    private Branch branch;

    public Vehicle(Date productionDate, VehicleType vehicleType, float mileage, String vinCode, String spz, String color, String maker, String model, float tankCapacity, Branch branch) {
        this.productionDate = productionDate;
        this.vehicleType = vehicleType;
        this.mileage = mileage;
        this.vinCode = vinCode;
        this.spz = spz;
        this.color = color;
        this.maker = maker;
        this.model = model;
        this.tankCapacity = tankCapacity;
        this.branch = branch;
    }
}
