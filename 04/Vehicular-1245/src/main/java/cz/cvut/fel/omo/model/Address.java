package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Getter
public class Address {
    private Country country;
    private String city;
    private String zip;
    private String street;
    private String streetNumber;
}
