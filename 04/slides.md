---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 04

Funkcionální programování
Pokračování objektového návrhu

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

- funkcionální programování v Javě
  - lambda výrazy
  - procedury vs funkce
  - a další srandy 😉
- pokračování objektového návrhu
  - [!] vzájemné rozbíjení GITu 😭
  - dokončení datových tříd (případně doma)

![center-right width:500](../media/git-exercise-meme.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Semestrálky

- v BRUTE jsou (snad) tři odevzdání
  - výběr tématu (a odkaz na repo)
  - analýza a detailní zadání
  - odevzdání
- nezapomeňte si nakliknout správně tým!
  - mají všichni tým?
  - pokud ne - bude mailová seznamka 🤯
![center-right width:600](../media/floor-is-java.webp)

## Lambda výrazy

- od Javy 8
- převzato z funkcionálního programování
  - `parametr -> výraz`
  - `parametr -> {blok kódu}`

```java
List<String> names = new LinkedList<>();
names.add("Petr");
names.add("Ondřej");
names.add("Xaver");

// Zápis for-each loopu objektově
for (String name : names) {
    System.out.println(name);
}

// Zápis for-each loopu funkcionálně
names.forEach(name -> System.out.println(name));
```

![top-right width:170](../media/lambda.gif)

## Lambda výrazy

- parametrů může být i více
  - zabalíme je do závorek
  - méně časté, ale stále používané

```java
Map<String, BigDecimal> salary = new HashMap<>();
salary.put("Petr", new BigDecimal(15000));
salary.put("Ondřej", new BigDecimal(50000));
salary.put("Xaver", new BigDecimal(35000));

// Zápis for-each loopu objektově
for (Entry<String, BigDecimal> entry : salary.entrySet()) {
    System.out.println(String.format("%s: %.2f Kč", entry.getKey(), entry.getValue()));
}

// Zápis for-each loopu funkcionálně
salary.forEach((name, value) -> {
    System.out.println(String.format("%s: %.2f Kč", name, value))
});
```

![top-right width:170](../media/lambda.gif)

## Lambda výrazy - \*Consumer

- třídy určené pro ukládání procedur do proměnných
  - `Consumer` pro proceduru s jedním parametrem
  - `BiConsumer` pro proceduru s dvěma parametry
- metodou `accept()` můžeme vykonat danou proceduru

```java
BiConsumer<Integer, Integer> multiply = (a, b) -> {
    System.out.println(String.format("%d*%d = %d", a, b, a * b));
};

List<Integer> numbers = List.of(5, 7, 2, 5, 1);
int i = 0;
for (Integer num : numbers) {
    multiply.accept(++i, num);
}
```

![bg right:30% width:420](../media/consumer.gif)

## Lambda výrazy - \*Function

- třídy určené pro ukládání funkcí do proměnných
  - `Function` pro proceduru s jedním parametrem
  - `BiFunction` pro proceduru s dvěma parametry
- metodou `apply()` můžeme vykonat danou proceduru

```java
BiFunction<Integer, Integer, Integer> multiply = (a, b) -> {
    return a * b;
};

List<Integer> numbers = List.of(5, 7, 2, 5, 1);
int i = 0;
for (Integer num : numbers) {
    System.out.println(multiply.apply(++i, num));
}
```

![bg right:30% width:420](../media/homer-huh.gif)

## Připomínky z přednášky

* co je to pure funkce?
  * funkce nemění a není měněna vnějším stavem tříd/objektů
* co je to currying?
  * vyhodnocení funkce "per partes"
  - 3 + 3 + 3 => 6 + 3 => 9
* co je to lazy evaluace?
  * funkce je vyhodnocena až ve chvíli, kdy si o to řeknu
  - příkladem jsou právě lambda výrazy

![bg right:30% width:420](../media/homer-hmm.gif)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Další objektový návrh

- utvořit dvojice
- (už vážně) vyzkoušet řešení konfliktů v GITu
- dále pokračovat na objektovém návrhu
