package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.Random;

@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Vehicle {
    private static Faker faker = new Faker(new Random(5));
    private final VehicleType type;
    private String serialNo;
    private String registrationNo;
    private int seatCount;
    private Date dateOfManufacturing;
    private Date dateOfPurchase;
    private boolean isRented;
    private float mileage;
    private float tankCapacity;
    private String maker;
    private String model;

    public static Vehicle generateRandom() {
        String serialNo = faker.idNumber().valid();


    }
}
