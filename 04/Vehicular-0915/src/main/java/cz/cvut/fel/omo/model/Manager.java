package cz.cvut.fel.omo.model;

import java.util.Date;

public class Manager extends Employee {
    protected final Date dateOfPromotion;

    public Manager(int id, String forename, String surname, Address address, Date creationDate, Date dateOfEmployment, int employeeNo, Date dateOfPromotion) {
        super(id, forename, surname, address, creationDate, dateOfEmployment, employeeNo);
        this.dateOfPromotion = dateOfPromotion;
    }
}
