package cz.cvut.fel.omo.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Set;

@EqualsAndHashCode
@ToString
@Getter
public final class Branch {
    private Manager manager;
    private Set<Employee> employees;
    private Set<Vehicle> vehicles;
    private Address address;
    private String name;
}
