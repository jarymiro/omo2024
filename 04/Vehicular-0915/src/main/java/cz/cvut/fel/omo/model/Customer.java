package cz.cvut.fel.omo.model;

import lombok.ToString;

import java.util.Date;

@ToString
public class Customer extends Person {
    protected final String customerCardNo;

    public Customer(int id, String forename, String surname, Address address, Date creationDate, String customerCardNo) {
        super(id, forename, surname, address, creationDate);
        this.customerCardNo = customerCardNo;
    }
}
