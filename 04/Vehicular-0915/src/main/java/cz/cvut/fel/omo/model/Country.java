package cz.cvut.fel.omo.model;

public enum Country {
    CZE("Czech Republic", "CZE"),
    SLK("Slovaika", "SLK"),
    PLN("Polsko", "PLN"),
    FRA("France", "FRA");

    private final String name;
    private final String code;

    Country(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
