package cz.cvut.fel.omo;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.*;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Address address = new Address("Praha", "Tekvicová", "55", Country.CZE, "555");
        System.out.println(address.toString());
        Faker faker = new Faker(new Random(5));
        com.github.javafaker.Address fakeAddress = faker.address();
        System.out.println(fakeAddress.city());
    }
}