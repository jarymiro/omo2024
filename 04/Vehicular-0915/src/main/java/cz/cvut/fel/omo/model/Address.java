package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@AllArgsConstructor
@ToString
public final class Address {
    private String city;
    private String street;
    private String houseNo;
    private Country country;
    private String postalCode;
}
