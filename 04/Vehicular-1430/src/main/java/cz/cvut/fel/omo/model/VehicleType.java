package cz.cvut.fel.omo.model;

public enum VehicleType {
    CAR,
    TRUCK,
    VAN,
    BOAT,
    AIRPLANE;
}
