package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Random;

@AllArgsConstructor
@ToString
@Setter
public final class Address {
    private static Faker faker = new Faker(new Random(5));
    private String city;
    private String street;
    private String houseNo;
    private Country country;
    private String zip;

    public static Address generateRandom() {
        com.github.javafaker.Address randomAddress = faker.address();
        String city = randomAddress.city();
        String street = randomAddress.streetAddress();
        String houseNo = randomAddress.buildingNumber();
        Country country = Country.CZECHIA;
        String zip = randomAddress.zipCode();

        return new Address(city, street, houseNo, country, zip);
    }
}
