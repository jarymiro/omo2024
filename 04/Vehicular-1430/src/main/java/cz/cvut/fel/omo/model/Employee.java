package cz.cvut.fel.omo.model;

import java.util.Date;

public class Employee extends Person {
    protected int employeeNo;
    protected final Date empolymentDate;

    public Employee(String forename, String surname, Address address, int employeeNo, Date empolymentDate) {
        super(forename, surname, address);
        this.employeeNo = employeeNo;
        this.empolymentDate = empolymentDate;
    }
}
