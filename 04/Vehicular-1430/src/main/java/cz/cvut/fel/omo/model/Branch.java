package cz.cvut.fel.omo.model;

import java.math.BigDecimal;
import java.util.List;

public class Branch {
    private BigDecimal fuelPrice;
    private Address address;
    private String name;
    private List<Employee> employees;
    private Manager manager;
    private List<Vehicle> vehicles;
}
