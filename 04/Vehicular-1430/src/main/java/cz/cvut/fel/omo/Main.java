package cz.cvut.fel.omo;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Country;
import cz.cvut.fel.omo.model.Person;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Address address = new Address("Praha", "Technická", "1903/2", Country.CZECHIA, "55502");
        System.out.println(address.toString());
        Person person;
        Faker faker = new Faker();
        System.out.println(faker.beer().name());

        System.out.println(Address.generateRandom());
        System.out.println(Address.generateRandom());
        System.out.println(Address.generateRandom());
    }
}