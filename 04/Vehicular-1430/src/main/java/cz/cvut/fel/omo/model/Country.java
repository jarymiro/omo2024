package cz.cvut.fel.omo.model;

public enum Country {
    SLOVAKIA("Slovakia", "svk"),
    CZECHIA("Czechia", "cze"),
    ROMANIA("Romania", "rom"),
    POLAND("Poland", "pln");

    private String name;
    private String code;

    Country(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
