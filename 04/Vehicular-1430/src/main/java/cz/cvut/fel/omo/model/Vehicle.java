package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Setter;

@Setter
@AllArgsConstructor
public class Vehicle {
    private String make;
    private String model;
    private int capacity;
    private double mass;
    private VehicleType type;
    private String callCode;
    private String color;
    private int modelYear;
    private double fuelCapacity;
    private Branch branch;


}
