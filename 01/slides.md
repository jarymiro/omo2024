---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 01

Informace o předmětu
opakování GITu

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## O předmětu

- deep-dive do objektového modelování a programování
  - jeden ze čtyř "druhů" programování
  - hojně využíváno v praxi
  - velkou část věnujeme tzv. Design patternům
- správný přístup k návrhu řešení problému
  - proč je dobré se před prací zamyslet?
  - jak přistupovat k řešení problému?
- další procvičení Javy!
  - případně Kotlinu (po dohodě)
- zkouška v režii přednášejícího

![bottom-right width:650](../media/java-shotgun.webp)

## Dostupné materiály

- primárně [CourseWare](https://cw.fel.cvut.cz/wiki/courses/b6b36omo/start)
  - informace k předmětu, úkoly, přednášky
- sekundárně [GitLab cvičícího](https://gitlab.fel.cvut.cz/jarymiro/omo2024)
  - prezentace a kód ze cvičení
- terciálně [Design Pattern Guide](https://refactoring.guru/design-patterns)
  - pro fajnšmekry [the GoF book](https://www.amazon.com/gp/product/0201633612/)

![top-right width:600](../media/dont-make-me-read.webp)

<!-- # Proč řešit objektový návrh?

![top-right width:500](../media/why-are-we-still-here.webp) -->

## Podmínky zápočtu

- vypracovat všechny čtyři [domácí úkoly](https://cw.fel.cvut.cz/wiki/courses/b6b36omo/hw/start)
  - zveřejněny v průběhu semestru
  - 5 bodů za včasné odevzdání
    (první je nebodován)
- vypracovat [semestrální práci](https://cw.fel.cvut.cz/wiki/courses/b6b36omo/hw/start#semestralni_prace)
  - odevzdání v posledních týdnech semestru
    (ideálně před Vánoci 😉)
- docházka na cvičení
  - max. tři neomluvené absence
  - omluvení absence nejdéle v pondělí večer!

![top-right width:600](../media/absence.webp)

## Hodnocení předmětu

<!-- _class: lead -->

| Kategorie             | Požadováno | Maximum |
| --------------------- | ---------- | ------- |
| Domácí úkoly          | 0          | 15      |
| Semestrální práce     | 0          | 25      |
| **Celkem za semestr** | **20**     | **40**  |
| Písemná zkouška       | 25         | 50      |
| _Ústní zkouška_       |            | 10      |
| **Celkem za předmět** | **50**     | **100** |

## Semestrální práce

- Java (Kotlin) projekt vypracovaný **ve dvojicích**
  - každý odvede ~stejný díl práce
- verzování za pomocí nástroje GIT
  (zopakujeme si dnes)
  - samotná práce s GITem je taktéž [hodnocena](https://docs.google.com/spreadsheets/d/1i3d6iiUiubS6MyhwksFV4zR4YdFMqlNn7nJglOKXdV8/edit?gid=0#gid=0)!
- výběr z [připravených zadání](https://cw.fel.cvut.cz/wiki/courses/b6b36omo/hw/start#semestralni_prace)
  - Smart Home
  - Smart Factory
  - Food Chain
  - DI Framework
- možnost konzultace v rámci semestru
  - dobrovolná, budou vypsány termíny

![center-right width:400](../media/teamwork.webp)

## Plagiátorství

- generativní AI (ChatGPT, Phind, Gemini...)
  - pouze v souladu s [pokyny ČVUT](https://www.cvut.cz/sites/default/files/content/d1dc93cd-5894-4521-b799-c7e715d3c59e/cs/20240130-metodicky-pokyn-c-52023.pdf)
- návody/tutoriály na internetu
  - ❌ pro objektový návrh/majoritu kódu
  - ✔️ pro pomocnou funkci, **ale s referencí**
- při odevzdání bude prověřeno a odhaleno

![top-right height:320](../media/ai-meme.webp)
![bottom-right height:320](../media/ai-meme.webp)

## GIT

- problém - jak spravovat codebase?
  - verzování (a případný rollback)
  - sdílení mezi vícero vývojářů
- primitivní řešení = OneDrive či jiný cloud
- my využijeme nástroj GIT
  - \*2005
  - https://git-scm.com/
  - ovládání pomocí příkazů
  - každé IDE má integrované GUI

![center-right width:520](../media/git-meme.webp)

## Repozitář

- = úložiště pro daný projekt
- obsahuje všechny informace o kódu a jeho změnách
- vývojář si může libovolné repo "naklonovat"
  - úpravy dělá ve své lokální kopii; následně je posílá na server
- přístup k repozitářům zpravidla dvěma protokoly
  - https - pomocí jména a hesla; snadné na nastavení, ale otravné
  - ssh - pomocí ssh klíče; déle se nastavuje, ale je bezpečnější

## Commit

- = snapshot stavu codebase,
  vytvořený vývojářem
- repozitář obsahuje historii všech commitů
- každý commit má
  - (dobře vystihující) název
  - univerzální identifikátor - hash
  - metadata (autor, datum...)
  - předky a potomky

![center-right width:600](../media/git-commit.webp)

## Branch a.k.a. větev

- = "pointer" na konkrétní commit v historii
- historie commitů totiž nemusí být lineární (a často taky není)
  - mám větev označenou jako "stabilní" a zbylé jako vývojové
  - každý vývojář má svou větev; nechtějí si vzájemně měnit kód

![bottom-center height:400](../media/git-branches.svg)

## Jak GIT funguje

![bg height:65%](../media/git-schema.webp)

## Základní příkazy v GITu

- **git clone <url>**
  - naklonuje repozitář z dané URL
  - v URL nesmí chybět protokol (https/ssh)
- **git init <cesta/ke/složce>**
  - inicializuje repozitář v dané složce
- **git status**
  - zobrazí aktuální stav repozitáře
  - na jaké jsem větvi, změněné soubory,
  konflikty...
  <!-- - **git remote set-url <název> <url>**
  - nastaví URL adresu ke vzdálenému serveru -->

![center-right width:680](../media/git-init.webp)

## Základní příkazy v GITu

- **git fetch**
  - stáhne stav serverového repozitáře
    do lokálního (nové větve, commity...)
- **git branch**
  - vypíše seznam existujících větví
- **git switch -c <nazev_vetve>**
  - vytvoří novou větev odštěpením z větve,
    na které jsme se nacházeli
- **git branch -D <nazev_vetve>**
  - odstraní větev
- **git pull**
  - aktualizuje větev na které se nacházíme
    do stavu, ve kterém je na serveru

![center-right width:580](../media/git-help.webp)

## Základní příkazy v GITu

- **git add <cesta/k/souboru>...**
  - přidá soubor k budoucímu commitu
- **git add --all**
  - přidá všechny soubory změněné
    od posledního commitu
- **git commit -m"Zpráva commitu"**
  - vytvoří commit obsahující výše přidané soubory
- **git commit -am"Zpráva commitu"**
  - vytvoří commit se všemi soubory
    změněnými od předchozího commitu
- **git push**
  - aktualizuje na serveru větev, na které
    se nacházíme, do stavu, ve kterém je u nás

![center-right width:580](../media/git-add.webp)

## Fakultní GitLab

- GitLab = git server s webovým rozhraním
- https://gitlab.fel.cvut.cz
- nabízí další nástroje pro vývoj
  - Wiki stránky
  - tvorba issues (pro organizaci práce)
  - pipelines (sada úkonů vykonávaných nad kódem - testování, nasazení...)

![top-right height:90](../media/gitlab.webp)

## Autentizace do GitLabu

- musíme nastavit přihlašování
  lokálního GITu do GitLabu
- preferovaný způsob přes **SSH klíč**
  - jednou nastavím a neřeším
  - `ssh-keygen`; následně složka `.ssh` v homedir
  - soubor s příponou `.pub` nahraji do GitLabu
    _\*profile pic\* > Preferences > SSH Keys_
- možnosti i přes https jméno a heslo
  - jako bych se přihlašoval přes web
  - považováno za méně bezpečné

![top-right width:600](../media/authentication.gif)

## Opáčko - Maven v IntelliJ IDEA

- File > New > Project
  - vyberte jazyk
  - groupId a artifactId schované
    pod "Advanced Settings"
- kompilace pod záložkou
  "Maven" vpravo

![center-right width:700](../media/new-project-idea.webp)

## Opáčko - Maven ve VSCode

- nainstalujte si
  "Extension Pack for Java"
- Explorer > Java Projects > +
- ve stejné sekci kompilace
- spuštění pomocí tlačítka "Run"

![center-right width:700](../media/new-project-vscode.webp)

## Opáčko - Maven v CLI

```sh
# Vytvoření projektu
mvn archetype:generate

# Kompilace všech tříd
# (do složky target/classes)
mvn compile

# Vytvoření JAR souboru
# (ve složce target)
mvn package
```

![top-right width:560](../media/hackerman.webp)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Inicializace projektu

- vytvořit GIT repozitář
- vytvořit Java projekt
- napsat si "Hello world!"
- práci nahrát na GIT
