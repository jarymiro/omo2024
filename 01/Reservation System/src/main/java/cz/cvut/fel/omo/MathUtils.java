package cz.cvut.fel.omo;

public class MathUtils {
    public MathUtils(){
        callCount = 0;
    }

    private int callCount;
    public int sumNumbers(int... nums){
        int sum = 0;
        StringBuilder stringBuilder = new StringBuilder();

        for(int num : nums){
            stringBuilder.append(num);
            stringBuilder.append(" + ");
            sum += num;
        }
        callCount+=1;
        stringBuilder.append(" = ");
        stringBuilder.append(sum);

        System.out.println(stringBuilder.toString());
        return sum;
    }
    public int minusNumbers(int num1, int num2){
        return num1 - num2;
    }
    public int getCallCount(){
        return callCount;
    }
}
