---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 10

Procvičování design patternů

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

* práce na společné codebase
* that's it... ![center-right width:600](../media/no-theory.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## A co teď?

* několik možností co implementovat
* každá paralelka si vybere to své
* výsledek off-screen pomerguju ![center-right width:600](../media/off-camera.webp)

## Task A: Person Factory

* [Factory Method](https://refactoring.guru/design-patterns/factory-method) pattern pro rodinu tříd `Person` ![center-right width:550](../media/task-a-meme.webp)
  - jednotliví potomci mají ale jiné vlastnosti;
    co s tím?
* zamyslet se, co bude ve společném rozhraní
  - metody na vytvoření nového objektu a objektu
    z faker dat

## Task B: Iterátor a TUI

* [Iterátor](https://refactoring.guru/design-patterns/iterator) pro kolekce v třídě `Branch` ![center-right width:550](../media/task-b-meme.webp)
  - řazení vzestupně, sestupně a náhodně
  - jaké společné metody budou mít všechny iterátory?
* implementace pro `Vehicle` a `Employee` kolekce
  - musím nutně vytvářet zvláštní třídy?

## Task C: Visitor a další FuP!

* rozhraní `DataAnalyzer` pro všechny [Visitory](https://refactoring.guru/design-patterns/visitor) ![center-right width:550](../media/task-c-meme.webp)
  - následně implementace `EmployeeDataAnalyzer`,
    `VehicleDataAnalyzer` a `CustomerDataAnalyzer`
* metody `processXY()` pro dané analyzátory
  - metody musí být deklarované ve společném rozhraní!
  - operace nad kolekcemi pomocí Stream API

## Task D: Code Smells 💩

* třída `Equipment` a seznam půjčeného v `Rental` ![center-right width:550](../media/task-d-meme.webp)
  - dalo by se nahradit nějakým Design Patternem? 🤔
  - případně přepracovat `List<VehicleType> rentableWith`?
* třídy `*Adapter` ve špatném namespacu
* počáteční konfigurace (data poboček/vozidel...)

## Další nápady?

![bg height:80%](../media/more-dp-meme.webp)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Vaše volba 😉

* vybrat si task viz předchozí slidy a implementovat
* případně obohacení aplikace o ne-OMO věci
  - pomocné metody
  - logování
  - serializace
* skončíme dřív? prostor pro konzultace