package cz.cvut.fel.omo.state;

public class IdleVehicleState extends VehicleState {
    @Override
    public VehicleState washVehicle() {
        return null;
    }

    @Override
    public VehicleState repairVehicle() {
        return null;
    }

    @Override
    public VehicleState returnVehicle() {
        return null;
    }

    @Override
    public VehicleState rentVehicle() {
        return null;
    }
}
