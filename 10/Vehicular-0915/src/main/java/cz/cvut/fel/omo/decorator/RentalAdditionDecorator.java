package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalFirstLayer;
import cz.cvut.fel.omo.model.RentalSecondLayer;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RentalAdditionDecorator implements RentalFirstLayer, RentalSecondLayer {
    protected RentalFirstLayer innerLayer;

    @Override
    public int getRentalDays() {
        return innerLayer.getRentalDays();
    }
}
