package cz.cvut.fel.omo;

import cz.cvut.fel.omo.configuration.BasicConfiguration;
import cz.cvut.fel.omo.configuration.Configuration;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new BasicConfiguration();
        configuration.run();
    }
}