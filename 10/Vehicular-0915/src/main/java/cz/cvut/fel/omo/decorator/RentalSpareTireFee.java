package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalSecondLayer;

import java.math.BigDecimal;

public class RentalSpareTireFee extends RentalMultiplyDecorator {
    public RentalSpareTireFee(RentalSecondLayer innerLayer) {
        super(innerLayer);
    }

    @Override
    public BigDecimal getPrice() {
        return innerLayer.getPrice().multiply(new BigDecimal(1.5));
    }
}
