package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import cz.cvut.fel.omo.state.IdleVehicleState;
import cz.cvut.fel.omo.state.VehicleState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@AllArgsConstructor


@Getter
@Setter
@ToString(exclude = "branch")
public class Vehicle implements Comparable {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    public static final Comparator<Vehicle> priceComparator = (a, b) -> {
        BigDecimal dailyPriceA = a.getDailyPrice();
        BigDecimal dailyPriceB = b.getDailyPrice();
        return dailyPriceA.compareTo(dailyPriceB);
    };

    private final VehicleType type;
    private String serialNo;
    private String registrationNo;
    private int seatCount;
    private Date dateOfManufacturing;
    private Date dateOfPurchase;
    private boolean isRented;
    private double mileage;
    private double tankCapacity;
    private String maker;
    private String model;
    private Branch branch;
    private VehicleState state;

    public void wash() {
        this.state = state.washVehicle();
    }

    public Vehicle(VehicleType type, String serialNo, String registrationNo, int seatCount, Date dateOfManufacturing, Date dateOfPurchase, boolean isRented, double mileage, double tankCapacity, String maker, String model, Branch branch) {
        this.type = type;
        this.serialNo = serialNo;
        this.registrationNo = registrationNo;
        this.seatCount = seatCount;
        this.dateOfManufacturing = dateOfManufacturing;
        this.dateOfPurchase = dateOfPurchase;
        this.isRented = isRented;
        this.mileage = mileage;
        this.tankCapacity = tankCapacity;
        this.maker = maker;
        this.model = model;
        this.branch = branch;
        this.state = new IdleVehicleState();
    }

    public static Vehicle generateRandom() {
        return new Vehicle(
                VehicleType.getRandom(),
                faker.idNumber().valid(),
                faker.idNumber().ssnValid(),
                faker.random().nextInt(8),
                faker.date().past(365 * 20, TimeUnit.DAYS),
                faker.date().past(30, TimeUnit.DAYS),
                false,
                faker.random().nextDouble() * 20000,
                faker.random().nextDouble() * 100,
                faker.company().name(),
                faker.company().buzzword(),
                null
        );
    }


    public Rental rentTo(Customer customer) {
        // TODO proper logic
        return null;
    }

    public BigDecimal getDailyPrice() {
        return branch.getFuelPrice()
                .multiply(new BigDecimal(tankCapacity / 4))
                .multiply(new BigDecimal(type.getMargin()));
    }


    @Override
    public int compareTo(Object arg0) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'compareTo'");
    }
}
