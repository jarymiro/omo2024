package cz.cvut.fel.omo.state;

public class InCarWashVehicleState extends VehicleState {
    @Override
    public VehicleState washVehicle() {
        return this;
    }

    @Override
    public VehicleState repairVehicle() {
        // branch.subtractFromAccount(1000);
        return new InServiceVehicleState();
    }

    @Override
    public VehicleState returnVehicle() {
        System.err.println("Vehicle got back from car wash");
        return new IdleVehicleState();
    }

    @Override
    public VehicleState rentVehicle() {
        return this;
    }
}
