package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalFirstLayer;

import java.math.BigDecimal;

public class RentalInsuranceFee extends RentalAdditionDecorator {
    public RentalInsuranceFee(RentalFirstLayer innerLayer) {
        super(innerLayer);
    }

    @Override
    public BigDecimal getPrice() {
        return innerLayer.getPrice().add(new BigDecimal(100));
    }
}
