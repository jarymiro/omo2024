package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Rental implements RentalFirstLayer, RentalSecondLayer {
    private Customer customer;
    private Vehicle vehicle;
    private Branch branch;
    private BigDecimal basePrice;
    private Date rentedFrom;
    private Date rentedTo;

    public BigDecimal getPrice() {
        return basePrice;
    }

    public int getRentalDays() {
        return 5; // TODO add proper calculation
    }
}
