package cz.cvut.fel.omo.model;

import lombok.*;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Rental implements RentalFirstInterface, RentalSecondInterface {
    private Customer customer;
    private Vehicle vehicle;
    private Branch branch;
    private BigDecimal totalPrice;
    private Date rentedFrom;
    private Date rentedTo;

    /**
     * Checks if this rental is currently ongoing based on system date
     *
     * @return true if yes, false otherwise
     */
    public boolean isActive() {
        Date dateNow = new Date();
        return dateNow.before(this.rentedTo) && dateNow.after(this.rentedFrom);
    }

    public BigDecimal getCalculatedTotalPrice() {
        return totalPrice;
    }

    @Override
    public long getRentalDays() {
        return ChronoUnit.DAYS.between(rentedFrom.toInstant(), rentedTo.toInstant());
    }
}
