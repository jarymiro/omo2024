package cz.cvut.fel.omo.factory;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import cz.cvut.fel.omo.model.VehicleType;
import lombok.AllArgsConstructor;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@AllArgsConstructor
public class VehicleFactory {
    private Branch branch;

    public Vehicle makeUsedCar(Date productionDate, double mileage, String vinCode, String spz, String color, String maker, String model, double tankCapacity) {
        return new Vehicle(
                productionDate,
                VehicleType.PERSONALCAR,
                mileage,
                vinCode,
                spz,
                color,
                maker,
                model,
                tankCapacity,
                this.branch
        );
    }

    public Vehicle makeNewCar(String vinCode, String spz, String color, String maker, String model, double tankCapacity) {
        return new Vehicle(
                new Date(),
                VehicleType.PERSONALCAR,
                0,
                vinCode,
                spz,
                color,
                maker,
                model,
                tankCapacity,
                this.branch);
    }

    public Vehicle makeNewVolvoTruck(String vinCode, String spz, String color, String model, double tankCapacity) {
        Vehicle vehicle = new Vehicle(
                new Date(),
                VehicleType.TRUCK,
                0,
                vinCode,
                spz,
                color,
                "Volvo",
                model,
                tankCapacity,
                this.branch);

        System.out.println(vehicle);
        return vehicle;
    }

    public Vehicle createNewFrontierBoat(String vinCode, String spz, String color, String model, double tankCapacity) {
        Vehicle b = new Vehicle(
                new Date(),
                VehicleType.BOAT,
                0,
                vinCode,
                spz,
                color,
                "Frontier",
                model,
                tankCapacity,
                this.branch
        );
        System.out.printf("Created new Frontier boat: %s%n", b);
        return b;
    }

    public Vehicle createFakeVehicle() {
        Faker faker = new Faker();
        Vehicle fakeVehicle = new Vehicle(
                faker.date().past(365 * 20, TimeUnit.DAYS),
                VehicleType.getRandom(),
                faker.random().nextDouble() * 20000,
                faker.idNumber().valid(),
                faker.idNumber().ssnValid(),
                faker.color().name(),
                faker.company().name(),
                faker.company().buzzword(),
                faker.random().nextDouble() * 100,
                null
        );
        System.out.printf("Fake Vehicle was created: %s%n", fakeVehicle);
        return fakeVehicle;
    }

}
