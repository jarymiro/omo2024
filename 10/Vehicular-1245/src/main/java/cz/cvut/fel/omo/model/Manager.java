package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.visitor.DataAnalyzer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString(callSuper = true)
public class Manager extends Employee {
    protected final Date promotionDate;

    // TODO method for creating Manager from Employee?

    public Manager(String username, String forename, String surname, Address address, Date employmentDate, Date promotionDate) {
        super(username, forename, surname, address, employmentDate);
        this.promotionDate = promotionDate;
    }

    public void accept(DataAnalyzer analyzer) {
        analyzer.processAdvanced(this);
    }
}
