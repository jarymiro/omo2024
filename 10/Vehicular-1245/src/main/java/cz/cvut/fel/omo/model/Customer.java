package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(callSuper = true)
public class Customer extends Person {
    protected String drivingLicenseNo;

    public Customer(String username, String forename, String surname, Address address, String drivingLicenseNo) {
        super(username, forename, surname, address);
        this.drivingLicenseNo = drivingLicenseNo;
    }

    public void receive(Notification notification) {
        System.out.printf("%s recieved notification: %s%n", this.username, notification.toString());
    }
}
