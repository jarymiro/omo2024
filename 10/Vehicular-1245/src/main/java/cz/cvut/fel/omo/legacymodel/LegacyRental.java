package cz.cvut.fel.omo.legacymodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class LegacyRental {
    private String uzivatelskeJmenoZakaznika;
    private String spzVozidla;
    private double cenaZaDen;
    private int pocetDni;

}
