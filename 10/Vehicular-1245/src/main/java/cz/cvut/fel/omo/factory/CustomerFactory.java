package cz.cvut.fel.omo.factory;

import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Person;
import lombok.AllArgsConstructor;

import java.util.Date;
import java.util.Map;

@AllArgsConstructor
public class CustomerFactory extends PersonFactory {
    protected Map<String, String> drivingLicenseDb;

    @Override
    public Person createFromData(String username, String forename, String surname, Address address, Date creationDate) {
        String drivingLicenseNo = drivingLicenseDb.get(username);
        if (drivingLicenseNo == null) {
            throw new IllegalStateException("Driver not found in db");
        }

        return new Customer(username, forename, surname, address, drivingLicenseNo);
    }

    @Override
    public Person createFromData(String username, String forename, String surname, Address address) {
        return this.createFromData(username, forename, surname, address, new Date());
    }

    @Override
    public Person createFromFakeData() {
        return new Customer(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.idNumber().valid()
        );
    }
}
