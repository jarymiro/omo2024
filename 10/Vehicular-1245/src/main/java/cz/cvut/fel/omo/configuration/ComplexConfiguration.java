package cz.cvut.fel.omo.configuration;

import org.apache.commons.lang3.NotImplementedException;

public class ComplexConfiguration extends Configuration {
    @Override
    protected void initalize() {
        throw new NotImplementedException("Do it in your own time ;)");
    }
}
