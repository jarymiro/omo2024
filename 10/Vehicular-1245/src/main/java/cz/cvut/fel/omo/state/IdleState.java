package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class IdleState extends VehicleState {
    public IdleState(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public Rental rentTo(Customer customer, Date rentedFrom, Date rentedTo) {
        this.nextState = new RentedState(vehicle);
        return new Rental(customer, this.vehicle, this.vehicle.getBranch(), new BigDecimal(100), rentedFrom, rentedTo);
    }

    @Override
    public void unrent(Rental rental) {
        this.nextState = this;
    }

    @Override
    public void fix() {
        this.nextState = new InServiceState(this.vehicle);
    }

    @Override
    public void wash() {
        this.nextState = new InCarWashState(this.vehicle);
    }
}
