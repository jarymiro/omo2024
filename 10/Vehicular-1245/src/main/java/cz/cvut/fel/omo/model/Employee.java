package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.visitor.DataAnalyzer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;


@Getter
@Setter
@ToString(callSuper = true)
public class Employee extends Person {
    private static int employeeCount = 0;
    protected int employeeNo;
    protected Date employmentDate;

    public Employee(String username, String forename, String surname, Address address, Date employmentDate) {
        // System.out.println("neco");
        super(username, forename, surname, address);
        this.employeeNo = ++employeeCount;
        this.employmentDate = employmentDate;
    }

    public Employee(String username, String forename, String surname, Address address, Date creationDate, Date employmentDate) {
        super(username, forename, surname, address, creationDate);
        this.employeeNo = ++employeeCount;
        this.employmentDate = employmentDate;
    }

    public void accept(DataAnalyzer analyzer) {
        analyzer.processBasic(this);
        analyzer.processAdvanced(this);
    }
}
