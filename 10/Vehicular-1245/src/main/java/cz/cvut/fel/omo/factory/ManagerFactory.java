package cz.cvut.fel.omo.factory;

import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Manager;
import cz.cvut.fel.omo.model.Person;
import org.apache.commons.lang3.NotImplementedException;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ManagerFactory extends PersonFactory {

    @Override
    public Person createFromData(String username, String forename, String surname, Address address, Date creationDate) {
        throw new NotImplementedException("Do it in your own time ;)");
    }

    @Override
    public Person createFromData(String username, String forename, String surname, Address address) {
        throw new NotImplementedException("Do it in your own time ;)");
    }

    @Override
    public Person createFromFakeData() {
        return new Manager(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS),
                faker.date().past(30, TimeUnit.DAYS)
        );
    }
}
