package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalFirstInterface;

import java.math.BigDecimal;

public class RentalExtraPassenger extends RentalFirstDecorator {
    public RentalExtraPassenger(RentalFirstInterface wrappedRental) {
        super(wrappedRental);
    }

    @Override
    public BigDecimal getCalculatedTotalPrice() {
        return wrappedRental.getCalculatedTotalPrice()
                .add(new BigDecimal(100));
    }
}
