package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.Date;
import java.util.List;


public abstract class VehicleState {
    protected final Vehicle vehicle;

    protected VehicleState nextState;

    public VehicleState(Vehicle vehicle) {
        this.vehicle = vehicle;
        this.nextState = null;
    }

    public abstract Rental rent(Customer customer, Date rentedFrom, Date rentedTo);

    public abstract void unrent();

    public abstract void repair();

    public abstract void wash();

    public abstract void crash();

    public VehicleState getNextState() {
        if (this.nextState == null) {
            return this;
        }
        return nextState;
    }
}
