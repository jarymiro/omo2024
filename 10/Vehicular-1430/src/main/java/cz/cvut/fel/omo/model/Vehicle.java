package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import cz.cvut.fel.omo.state.UnrentedVehicleState;
import cz.cvut.fel.omo.state.VehicleState;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Getter
@Setter
@ToString(exclude = "branch")
@AllArgsConstructor
public class Vehicle {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    public static final Comparator<Vehicle> priceComparator = (a, b) -> a.getDailyPrice().compareTo(b.getDailyPrice());

    private String make;
    private String model;
    private int capacity;
    private double mass;
    private VehicleType type;
    private String callCode;
    private String color;
    private int modelYear;
    private double fuelCapacity;
    private Branch branch;
    private VehicleState state;

    public Vehicle(String make, String model, int capacity, double mass, VehicleType type, String callCode, String color, int modelYear, double fuelCapacity, Branch branch) {
        this.make = make;
        this.model = model;
        this.capacity = capacity;
        this.mass = mass;
        this.type = type;
        this.callCode = callCode;
        this.color = color;
        this.modelYear = modelYear;
        this.fuelCapacity = fuelCapacity;
        this.branch = branch;
        this.state = new UnrentedVehicleState(this);
    }

    public static Vehicle generateRandom() {
        return new Vehicle(
                faker.company().name(),
                faker.company().buzzword(),
                faker.random().nextInt(8),
                1000 + faker.random().nextDouble() * 5000,
                VehicleType.getRandom(),
                faker.idNumber().ssnValid(),
                faker.color().name(),
                faker.random().nextInt(1960, 2024),
                faker.random().nextDouble() * 100,
                null
        );
    }

    public Rental rentTo(Customer customer, List<Equipment> equipment, Date rentedFrom, Date rentedTo) {
        Rental rental = state.rent(customer, equipment, rentedFrom, rentedTo);
        this.state = state.getNextState();
        return rental;
    }

    public BigDecimal getDailyPrice() {
        return branch.getFuelPrice()
                .multiply(new BigDecimal(fuelCapacity / 4))
                .multiply(BigDecimal.valueOf(type.getMargin()));
    }

}
