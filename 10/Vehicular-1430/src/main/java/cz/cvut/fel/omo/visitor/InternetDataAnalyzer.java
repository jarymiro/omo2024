package cz.cvut.fel.omo.visitor;

import cz.cvut.fel.omo.model.Employee;

public class InternetDataAnalyzer extends DataAnalyzer {
    @Override
    public void processBasic(Employee employee) {
        System.out.printf("Checking data of %s %s in the internet\n", employee.getForename(), employee.getSurname());
    }
}
