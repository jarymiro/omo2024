package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.visitor.DataAnalyzer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(callSuper = true)
public class Employee extends Person {
    protected final Date empolymentDate;

    public Employee(String forename, String surname, Address address, Date empolymentDate) {
        super(forename, surname, address);
        this.empolymentDate = empolymentDate;
    }

    public static Employee generateRandom() {
        return new Employee(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS)
        );
    }

    public void accept(DataAnalyzer analyzer) {
        analyzer.processBasic(this);
        analyzer.processAdvanced(this);
    }
}
