package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Random;

@AllArgsConstructor
@Getter
public enum VehicleType {
    CAR(1.5),
    TRUCK(1.6),
    VAN(1.6),
    BOAT(1.8),
    AIRPLANE(2.2);

    private final double margin;

    private static final Random random = new Random(Constants.RANDOM_SEED);

    public static VehicleType getRandom() {
        return values()[random.nextInt(values().length)];
    }
}
