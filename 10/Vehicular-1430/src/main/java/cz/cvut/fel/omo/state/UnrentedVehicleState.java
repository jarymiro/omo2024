package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;
import org.apache.commons.lang3.NotImplementedException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class UnrentedVehicleState extends VehicleState {
    public UnrentedVehicleState(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public Rental rent(Customer customer, Date rentedFrom, Date rentedTo) {
        // TODO fix with correct day count
        Rental rental = new Rental(customer, this.vehicle, this.vehicle.getBranch(), vehicle.getDailyPrice().multiply(new BigDecimal(5)), rentedFrom, rentedTo);
        this.nextState = new RentedVehicleState(this.vehicle);
        return rental;
    }

    @Override
    public void unrent() {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void repair() {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void wash() {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void crash() {
        throw new NotImplementedException("Do in your free time!");
    }
}
