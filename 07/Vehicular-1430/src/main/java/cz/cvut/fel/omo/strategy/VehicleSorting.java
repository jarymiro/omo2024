package cz.cvut.fel.omo.strategy;

import java.util.List;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class VehicleSorting {
    protected Branch branch;

    public abstract List<Vehicle> getSorted();
}
