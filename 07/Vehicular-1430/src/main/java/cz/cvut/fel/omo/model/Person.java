package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Random;

@Getter
@Setter
@ToString
public abstract class Person {
    protected static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));
    private static int count = 0;

    protected final int id;
    protected String forename;
    protected String surname;
    protected Address address;
    protected Date creationDate;

    public Person(int id, String forename, String surname, Address address, Date creationDate) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
        this.address = address;
        this.creationDate = creationDate;
    }

    public Person(String forename, String surname, Address address) {
        this(++count, forename, surname, address, new Date());
    }
}
