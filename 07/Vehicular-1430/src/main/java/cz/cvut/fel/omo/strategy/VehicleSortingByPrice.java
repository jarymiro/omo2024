package cz.cvut.fel.omo.strategy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

public class VehicleSortingByPrice extends VehicleSorting {

    public VehicleSortingByPrice(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = new ArrayList<>(branch.getVehicles());
        
        vehicles.sort((a, b) -> {
            return a.getDailyPrice().compareTo(b.getDailyPrice());
            // if (a.getCapacity() > b.getCapacity()) {
            //     return 1;
            // } else if (b.getCapacity() > a.getCapacity()) {
            //     return -1;
            // } else {
            //     return 0;
            // }
        });

        return vehicles;
    }

    
}
