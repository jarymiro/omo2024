package cz.cvut.fel.omo.factory;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import cz.cvut.fel.omo.model.VehicleType;
import lombok.AllArgsConstructor;

import java.util.Date;

@AllArgsConstructor
public class VehicleFactory {
    private String make;

    public Vehicle makeUsedCar(String model, int capacity, double mass, String callCode, String color, int modelYear, double fuelCapacity, Branch branch) {
        return new Vehicle(
                this.make,
                model,
                capacity,
                mass,
                VehicleType.CAR,
                callCode,
                color,
                modelYear,
                fuelCapacity,
                branch
        );
    }

    public Vehicle makeNewCar(String model, int capacity, double mass, String callCode, String color, double fuelCapacity, Branch branch) {
        return new Vehicle(
                this.make,
                model,
                capacity,
                mass,
                VehicleType.CAR,
                callCode,
                color,
                (new Date()).getYear(),
                fuelCapacity,
                branch
        );
    }

    public Vehicle makeNewBrandedCar(String model, int capacity, double mass, String callCode, double fuelCapacity, Branch branch) {
        Vehicle vehicle = new Vehicle(
                this.make,
                model,
                capacity,
                mass,
                VehicleType.CAR,
                callCode,
                "purple",
                (new Date()).getYear(),
                fuelCapacity,
                branch
        );

        System.out.printf("New branded car: %s%n", vehicle);
        return vehicle;
    }

    public Vehicle createFakeVehicle() {
        Faker faker = new Faker();
        Vehicle fakeVehicle = new Vehicle(
                faker.company().name(),
                faker.company().buzzword(),
                faker.random().nextInt(8),
                1000 + faker.random().nextDouble() * 5000,
                VehicleType.getRandom(),
                faker.idNumber().ssnValid(),
                faker.color().name(),
                faker.random().nextInt(1960, 2024),
                faker.random().nextDouble() * 100,
                null
        );
        System.err.printf("Fake Vehicle was created: %s%n", fakeVehicle);
        return fakeVehicle;
    }


}
