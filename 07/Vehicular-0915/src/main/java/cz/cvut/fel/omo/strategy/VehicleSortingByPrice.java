package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class VehicleSortingByPrice extends VehicleSorting {
    public VehicleSortingByPrice(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = branch.getVehicles()
                .stream()
                .sorted((a, b) -> {
                    BigDecimal dailyPriceA = a.getDailyPrice();
                    BigDecimal dailyPriceB = b.getDailyPrice();
                    return dailyPriceA.compareTo(dailyPriceB);
                })
                .toList();

        return vehicles;
    }
}
