package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Random;

@AllArgsConstructor
@Getter
@Setter
@ToString
public abstract class Person {
    protected static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    protected String forename;
    protected String surname;
    protected Address address;
    protected Date creationDate;
}
