package cz.cvut.fel.omo.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.*;

@AllArgsConstructor
@Getter
@Setter
@ToString(exclude = "vehicles")
public final class Branch {
    private Manager manager;
    private final Set<Employee> employees;
    private final Set<Vehicle> vehicles;
    private final Set<Rental> rentals;
    private Address address;
    private String name;
    private BigDecimal fuelPrice;
    private List<Customer> subscribers;

    public Branch(Manager manager, Address address, String name, BigDecimal fuelPrice) {
        this(manager, new HashSet<>(), new HashSet<>(), new HashSet<>(), address, name, fuelPrice, new ArrayList<>());
    }

    /**
     * Employ provided person in this branch
     *
     * @param person
     */
    public void employPerson(Person person) {
        // TODO proper logic
    }

    /**
     * Promote provided employee to a manager of the branch.
     *
     * @param employee
     */
    public void promoteToManager(Employee employee) {
        // TODO proper logic
    }

    /**
     * Add provided vehicle to the branch's fleet
     *
     * @param vehicle
     */
    public void addVehicleToFleet(Vehicle vehicle) {
        if (vehicle.getBranch() == null) {
            this.vehicles.add(vehicle);
            vehicle.setBranch(this);
            for (Customer customer : subscribers) {
                customer.receiveNotification(new Notification(this.name, "Added new Auto!!!", new Date()));
            }
        } else {
            System.err.println("Vehicle is already in branch!");
        }
    }

    public void subscribe(Customer customer) {
        this.subscribers.add(customer);
    }

    public void unsubscribe(Customer customer) {
        this.subscribers.remove(customer);
    }
}
