package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


@Getter
@Setter
@ToString
@AllArgsConstructor
public class Equipment {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private String name;
    private BigDecimal dailyPrice;
    private List<VehicleType> rentableWith;

    public boolean isRentableWith(VehicleType vehicleType) {
        return this.rentableWith.contains(vehicleType);
    }

    public Equipment generateRandom() {
        return new Equipment(
                faker.commerce().productName(),
                BigDecimal.valueOf(faker.random().nextDouble() * 30000),
                Arrays.asList(VehicleType.values())
        );
    }
}
