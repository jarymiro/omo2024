---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 07

Strategy
Facade, Decorator a Proxy

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

* behaviorální design patterny
  - Strategy
* strukturální design patterny
  - Facade
  - Decorator
  - Proxy
* dotazy k zadání 3. úkolu
* zapracování patternů do kódu viz výše

![center-right width:700](../media/facade-meme.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Rychlá koordinačka

![center-right height:80%](../media/we-need-to-talk.webp)

* málo lidí se ozvalo na bonusový úkol
  - špatný týden? (zápočťáky)
  - špatný koncept?
  - špatný rozsah?
* [Google Forms](https://docs.google.com/forms/d/e/1FAIpQLSecBw_qvX8PZb91hB9G4FnrugM2TF3KAc_4N6wo3W3KRFcyjA/viewform?usp=sf_link) pro zaslání odkazu na repo SP
  - zatím vyplnilo 12 týmu - to je podezřele málo 🤨
* [Google Calendar](https://calendar.app.google/JK53sYoufxncqpnx7) pro zamluvení konzultace na SP
  - dobrovolné, dle libosti
  - preferuji offline, po domluvě lze ale i online
  - choďte připraveni!

## [Strategy](https://refactoring.guru/design-patterns/strategy) - když víš co, ale ne jak

* problém: máme akci, kterou lze
  udělat mnoha způsoby
  - např. plánování tras v mapě
  - běžně bychom udělali obrovský if-blok
    a měli všechen kód v jedné třídě
  - nepřehledné, musíme řešit merge konflikty...
* řešení bez if-bloků = Strategy
  ![top-right width:750](../media/uml-strategy.webp)
  - zadefinujeme si společného předka pro všechny způsoby
  - každý potomek = jeden způsob
* další příklady
  - seřazení objektů v poli
  - exportér dat do souboru (CSV/JSON...)

## [Strategy](https://refactoring.guru/design-patterns/strategy) - když víš co, ale ne jak

máme navigaci a plánování trasy pro auta a chodce

```java
class Navigator {
  // reference na RouteStrategy
  // metoda na využívání RouteStrategy
}

interface RouteStrategy {
  public Road buildRoad(Point source, Point destination);
}
```

![bg right:37% width:95%](../media/navigation-meme.webp)

## [Strategy](https://refactoring.guru/design-patterns/strategy) - když víš co, ale ne jak

máme navigaci a plánování trasy pro auta a chodce

```java
interface RouteStrategy {
  public Road buildRoad(Point source, Point destination);
}

class CarRouteStrategy implements RouteStrategy {
  // Co bude zde?
}
class WalkingRouteStrategy implements RouteStrategy {
  // Co bude zde?
}
```

![bg right:37% width:95%](../media/navigation-meme.webp)

## [Strategy](https://refactoring.guru/design-patterns/strategy) - když víš co, ale ne jak

máme navigaci a plánování trasy pro auta a chodce

```java
interface RouteStrategy {
  public Road buildRoad(Point source, Point destination);
}

class CarRouteStrategy implements RouteStrategy {
  public Road buildRoad(Point source, Point destination) {
    // Vytvoř trasu za pomocí silnic a dálnic
  }
}
class WalkingRouteStrategy implements RouteStrategy {
  public Road buildRoad(Point source, Point destination) {
    // Vytvoř trasu za pomocí pěších zón a parků
  }
}
```

![bg right:37% width:95%](../media/navigation-meme.webp)

## [Facade](https://refactoring.guru/design-patterns/facade) - když chceš skrýt složité

* problém: máme složitý kód, pro který
  chceme jednoduché rozhraní
  - knihovna třetí strany, která má
    složité objekty, a my potřebujeme jen část
* řešení = facade
  ![top-right width:650](../media/uml-facade.webp)
  - podstatné co potřebujeme uklidíme do jedné třídy
* vzdáváme se tím však původních funkcionalit!
  - kompromis mezi jednoduchostí a použitelností

## [Facade](https://refactoring.guru/design-patterns/facade) - když chceš skrýt složité

složitou logiku knihovny schováme do třídy `VideoConverter`
(za cenu menší univerzálnosti)

```java
abstract class Codec {}
class Mpeg2Codec extends Codec {}
class Mpeg4Code extends Codec {}
class CodecFactory {}

abstract class VideoContainer {}
class Mp4VideoContainer extends VideoContainer {}
class AviVideoContainer extends VideoContainer {}
class WebMVideoContainer extends VideoContainer {}

class VideoConverter {
  public Video convertToMp4(String src, String dest) {
    ...
  }
}
```

## [Decorator](https://refactoring.guru/design-patterns/decorator) - když chceš složitě rozšiřovat

* problém: chceme obohatit třídu různými způsoby
  - banka může na převod peněz aplikovat různé poplatky
    (dle typu transakce)
  - nevyřešíme obyčejnou dědičností
* řešení = decorator
  ![top-right width:540](../media/uml-decorator.webp)
  - vlastnosti budeme "řetězit" pomocí konstruktoru
  - všechna volání budou propadat skrz vrstvy
  - nezáleží na pořadí řetězení
  - poslední vrstva = samotná core logika kódu
* takto jsou v Javě implementované IO Streamy

## [Decorator](https://refactoring.guru/design-patterns/decorator) - když chceš složitě rozšiřovat

máme společné rozhraní `Transaction` a různé poplatky pro provedení transakce

```java
public interface Transaction {
  public boolean setRecipient(String username);
  public boolean setAmount(BigDecimal amount);
}

public class BankAccountTransaction implements Transaction {
  public boolean setRecipient(String username) { ... }
  public boolean setAmount(BigDecimal amount) { ... }
}

public class TransactionDecorator implements Transaction {
  // Ulož předchozí zřetězený Transaction objekt
  // Jak bude vypadat metoda setRecipient()?
  // Jak bude vypadat metoda setAmount()?
}
```

![bg right:26% width:96%](../media/bank-meme.webp)

## [Decorator](https://refactoring.guru/design-patterns/decorator) - když chceš složitě rozšiřovat

máme společné rozhraní `Transaction` a různé poplatky pro provedení transakce

```java
public class BankAccountTransaction implements Transaction {
  public boolean setRecipient(String username) { ... }
  public boolean setAmount(BigDecimal amount) { ... }
}

@AllArgsConstructor
public class TransactionDecorator implements Transaction {
  protected Transaction transaction;
  public boolean setRecipient(String username) { return transaction.setRecipient(username); }
  public boolean setAmount(BigDecimal amount) { return transaction.setAmount(amount); }
}

@AllArgsConstructor
public class TransactionBasicFeeDecorator extends TransactionDecorator {
  // Jak bude vypadat metoda setRecipient()?
  // Jak bude vypadat metoda setAmount()?
}
```

![bg right:26% width:96%](../media/bank-meme.webp)

## [Decorator](https://refactoring.guru/design-patterns/decorator) - když chceš složitě rozšiřovat

máme společné rozhraní `Transaction` a různé poplatky pro provedení transakce

```java
public class BankAccountTransaction implements Transaction {
  public boolean setRecipient(String username) { ... }
  public boolean setAmount(BigDecimal amount) { ... }
}

public class TransactionDecorator implements Transaction {
  protected Transaction transaction;
  public boolean setRecipient(String username) { return transaction.setRecipient(username); }
  public boolean setAmount(BigDecimal amount) { return transaction.setAmount(amount); }
}

public class TransactionBasicFeeDecorator extends TransactionDecorator {
  protected Transaction transaction;
  public boolean setRecipient(String username) { return transaction.setRecipient(username); }
  public boolean setAmount(BigDecimal amount) { return transaction.setAmount(amount * 0.95); }
}
```

![bg right:26% width:96%](../media/bank-meme.webp)

## [Proxy](https://refactoring.guru/design-patterns/proxy) - když chceš obalit cizí

* problém: potřebujeme obalit volání nějaké třídy
  - zalogování volání u objektu který nemůžu modifikovat
* řešení = proxy
  ![top-right width:520](../media/uml-proxy.webp)
  - zdědím společného předka původní třídy
  - původní třídu si uložím jako vlastnost nové
  - v implementaci metod volám původní třídu
    (jako v adaptéru)
* další příklady
  - obalím volání a zkontroluji autorizaci/práva
  - cachování/lazy inicializace

## [Proxy](https://refactoring.guru/design-patterns/proxy) - když chceš obalit cizí

máme knihovnu třetí strany, která volá externí API,
a které chceme cachovat

```java
interface ThirdPartyLib {
  public List<Course> getCourses();
  public List<Student> getStudents();
  public List<Student> getStudentById(int id);
}

class ThirdPartyLibClass implements ThirdPartyLib {
  public List<Course> getCourses() { ... }
  public List<Student> getStudents() { ... }
  public List<Student> getStudentById(int id) { ... }
}
```

![bg right:32% width:96%](../media/proxy-meme.webp)

## [Proxy](https://refactoring.guru/design-patterns/proxy) - když chceš obalit cizí

máme knihovnu třetí strany, která volá externí API,
a které chceme cachovat

```java
class ThirdPartyLibClass implements ThirdPartyLib {
  public List<Course> getCourses() { ... }
  public List<Student> getStudents() { ... }
  public List<Student> getStudentById(int id) { ... }
}


class CachedThirdPartyLibClass implements ThirdPartyLib {
  protected Cache cache; // Metody exists(key), get(key), set(key)
  // Jak bude vypadat reference na originální implementaci třídy?

  // Jak bude vypadat metoda getCourses s cachováním?
}
```

![bg right:32% width:96%](../media/proxy-meme.webp)

## [Proxy](https://refactoring.guru/design-patterns/proxy) - když chceš obalit cizí

máme knihovnu třetí strany, která volá externí API,
a které chceme cachovat

```java
class ThirdPartyLibClass implements ThirdPartyLib {
  public List<Course> getCourses() { ... }
  public List<Student> getStudents() { ... }
  public List<Student> getStudentById(int id) { ... }
}


class CachedThirdPartyLibClass implements ThirdPartyLib {
  protected Cache cache; // Metody exists(key), get(key), set(key, value)
  protected ThirdPartyLib dataSource;

  // Jak bude vypadat metoda getCourses s cachováním?
}
```

![bg right:32% width:96%](../media/proxy-meme.webp)

## [Proxy](https://refactoring.guru/design-patterns/proxy) - když chceš obalit cizí

máme knihovnu třetí strany, která volá externí API,
a které chceme cachovat

```java
@AllArgsConstructor
class CachedThirdPartyLibClass implements ThirdPartyLib {
  protected Cache cache; // Metody exists(key), get(key), set(key, value)
  protected ThirdPartyLib dataSource;

  public List<Course> getCourses() {
    String key = "courses";
    List<Course> courses;
    if (cache.exists(key)) {
      courses = cache.get(key);
    } else {
      courses = dataSource.getCourses();
      cache.set(key, courses);
    }
    return courses;
  }
}
```

![bg right:32% width:96%](../media/proxy-meme.webp)

## [Adaptér](https://refactoring.guru/design-patterns/adapter) vs. [Facade](https://refactoring.guru/design-patterns/facade) vs. [Decorator](https://refactoring.guru/design-patterns/decorator) vs. [Proxy](https://refactoring.guru/design-patterns/proxy)

![center-right width:440](../media/structural-mashup.webp)

* Adaptér má jiné rozhraní, spojuje ho s existujícím
  - přepoužití legacy tříd v novém kódu
* Facade má jiné rozhraní, zjednodušuje ho do nového
  - skrytí složité knihovny do jedné třídy
* Decorator má obohacené rozhraní, řetězí původní
  - vrstevnaté přidání funkcí do cizí třídy
* Proxy má stejné rozhraní, obohacuje ho
  - přidání logování do cizí třídy

## [3. domácí úkol](https://cw.fel.cvut.cz/b241/courses/b6b36omo/hw03)

![top-right width:650](../media/hw3-meme.webp)

* deadline 24. 11.
* implementace Facade a Strategy patternů
* využijte připravenou [šablonu](https://cw.fel.cvut.cz/b241/_media/courses/b6b36omo/omo-hw3-students_1.zip)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Strategy a Decorator

* vytvořit abstraktní třídu **VehicleSorting**
  - v balíčku **cz.cvut.fel.omo.strategy**
  - parametr pobočky ze které bude získávat seznam vozidel
  - **VehicleSortingByPrice** (cena půjčky) a **VehicleSortingBySize** (počet míst)
* implementovat dekorátor pro třídu **Rental**
  - **RentalExtraFuel** - dostanu půlku nádrže navíc za výhodnou cenu
  - **RentalPersonalDriver** - k vozu dostanu i řidiče
