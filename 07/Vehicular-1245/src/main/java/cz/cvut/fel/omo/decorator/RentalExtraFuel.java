package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalInterface;

import java.math.BigDecimal;

public class RentalExtraFuel extends RentalDecorator {
    public RentalExtraFuel(RentalInterface wrappedRental) {
        super(wrappedRental);
    }

    @Override
    public BigDecimal getCalculatedTotalPrice() {
        return wrappedRental.getCalculatedTotalPrice()
                .multiply(new BigDecimal(2));
    }
}
