package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalInterface;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RentalDecorator implements RentalInterface {
    protected RentalInterface wrappedRental;
}
