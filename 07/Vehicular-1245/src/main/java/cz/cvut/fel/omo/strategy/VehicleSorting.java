package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public abstract class VehicleSorting {
    protected Branch branch;

    public abstract List<Vehicle> getSortedVehicles();
}
