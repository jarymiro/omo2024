package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(exclude = "branch")
public class Vehicle {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private VehicleType vehicleType;
    private double mileage;
    private final Date productionDate;
    private String vinCode;
    private String spz;
    private String color;
    private String maker;
    private String model;
    private double tankCapacity;
    private Branch branch;

    public Vehicle(Date productionDate, VehicleType vehicleType, double mileage, String vinCode, String spz, String color, String maker, String model, double tankCapacity, Branch branch) {
        this.productionDate = productionDate;
        this.vehicleType = vehicleType;
        this.mileage = mileage;
        this.vinCode = vinCode;
        this.spz = spz;
        this.color = color;
        this.maker = maker;
        this.model = model;
        this.tankCapacity = tankCapacity;
        this.branch = branch;
    }

    public static Vehicle generateRandom() {
        return new Vehicle(
                faker.date().past(365 * 20, TimeUnit.DAYS),
                VehicleType.getRandom(),
                faker.random().nextDouble() * 20000,
                faker.idNumber().valid(),
                faker.idNumber().ssnValid(),
                faker.color().name(),
                faker.company().name(),
                faker.company().buzzword(),
                faker.random().nextDouble() * 100,
                null
        );
    }

    public Rental rentTo(Customer customer) {
        // TODO proper logic
        return null;
    }

    public BigDecimal getDailyPrice() {
        return branch.getFuelPrice()
                .multiply(new BigDecimal(tankCapacity / 4))
                .multiply(new BigDecimal(vehicleType.getMargin()));
    }
}
