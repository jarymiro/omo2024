package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Branch {
    private Address address;
    private Set<Vehicle> vehicles;
    private List<Employee> employees;
    private Manager manager;
    private BigDecimal fuelPrice;
    private List<Customer> subscribers;

    public Branch(Address address, Manager manager, BigDecimal fuelPrice) {
        this(address, new HashSet<>(), new ArrayList<>(), manager, fuelPrice, new ArrayList<>());
    }


    /**
     * Employ provided person in this branch
     *
     * @param person
     */
    public void employPerson(Person person) {
        // TODO proper logic
    }

    /**
     * Promote provided employee to a manager of the branch.
     *
     * @param employee
     */
    public void promoteToManager(Employee employee) {
        // TODO proper logic
    }

    /**
     * Add provided vehicle to the branch's fleet
     *
     * @param vehicle
     */
    public void addVehicleToFleet(Vehicle vehicle) {
        if (vehicle.getBranch() == null) {
            vehicles.add(vehicle);
            vehicle.setBranch(this);
            Notification notification = new Notification(address.toString(), String.format("%s NEW AMAZING CAR WAS ADDED TO OUR BRANCH %s, SO COME AND TEST IT!", vehicle, address.toString()));
            informSubscribers(notification);
        } else {
            System.err.println("This vehicle is already in the branch");
        }
    }

    public void addSubscriber(Customer customer) {
        subscribers.add(customer);
    }

    public void removeSubscriber(Customer customer) {
        subscribers.remove(customer);
    }

    private void informSubscribers(Notification notification) {
        for (Customer customer : subscribers) {
            customer.receive(notification);
        }
    }
}
