package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;

public class FakeCustomerAdapter extends Customer {
    private static final Faker faker = new Faker();

    public FakeCustomerAdapter() {
        super(
                faker.name().username(),
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.idNumber().valid()
        );
    }
}
