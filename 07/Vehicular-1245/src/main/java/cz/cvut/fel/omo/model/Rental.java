package cz.cvut.fel.omo.model;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Rental implements RentalInterface {
    private Customer customer;
    private Vehicle vehicle;
    private List<Equipment> equipment;
    private Branch branch;
    private BigDecimal totalPrice;
    private Date rentedFrom;
    private Date rentedTo;

    /**
     * Checks if this rental is currently ongoing based on system date
     *
     * @return true if yes, false otherwise
     */
    public boolean isActive() {
        Date dateNow = new Date();
        return dateNow.before(this.rentedTo) && dateNow.after(this.rentedFrom);
    }

    public BigDecimal getCalculatedTotalPrice() {
        return totalPrice;
    }
}
