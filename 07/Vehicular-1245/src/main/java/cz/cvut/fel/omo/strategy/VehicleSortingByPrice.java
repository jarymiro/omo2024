package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.math.BigDecimal;
import java.util.List;

public class VehicleSortingByPrice extends VehicleSorting {
    public VehicleSortingByPrice(Branch branch) {
        super(branch);
    }

    public List<Vehicle> getSortedVehicles() {
        return branch.getVehicles().stream()
                .sorted((a, b) -> {
                    BigDecimal dailyPriceA = a.getDailyPrice();
                    BigDecimal dailyPriceB = b.getDailyPrice();
                    return dailyPriceA.compareTo(dailyPriceB);
                }).toList();
    }
}
