package cz.cvut.fel.omo.model;

import java.math.BigDecimal;

public interface RentalInterface {
    public BigDecimal getCalculatedTotalPrice();
}
