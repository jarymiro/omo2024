package cz.cvut.fel.omo.legacymodel;

import cz.cvut.fel.omo.model.Address;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LegacyCzechAddressAdapter extends Address {

    private LegacyCzechAddress legacyCzechAddress;

    @Override
    public String toString() {
        return String.format("%s %d/%d, %s", legacyCzechAddress.getUlice(),
                legacyCzechAddress.getCisloPopisne(),
                legacyCzechAddress.getCisloOrientacni(),
                legacyCzechAddress.getMesto());
    }
}
