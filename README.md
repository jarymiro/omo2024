# B6B36OMO - Objektový návrh a modelování (B241)

Materiály ke cvičení z předmětu OMO pro paralelky 103, 104 a 105.

Materiály z přednášek naleznete [na stránkách CourseWare](https://cw.fel.cvut.cz/b241/courses/b6b36omo).

Ke konzultacím/odevzdáním semestrální práce se můžete přihlásit [ZDE](https://calendar.google.com/calendar/appointments/AcZssZ34SG1byLAHcxS801_MuN4U1pa_rjgn3HKO4tY=).

## Prezentace

| Číslo | Téma cvičení                                                  | Odkaz na prezentaci                                              |
| ----- | ------------------------------------------------------------- | ---------------------------------------------------------------- |
| 01    | Průběh předmětu, opakování GITu                               | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/01/slides.html) |
| 02    | Opakování Javy, Základní objektový návrh                      | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/02/slides.html) |
| 03    | (Do)opakování Javy, Základní objektový návrh                  | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/03/slides.html) |
| 04    | ~~Funkcionální programování~~, pokračování objektového návrhu | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/04/slides.html) |
| 05    | Singleton, Adaptér & Template Method                          | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/05/slides.html) |
| 06    | Factories & Observer                                          | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/06/slides.html) |
| 07    | Strategy, Facade, Decorator a Proxy                           | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/07/slides.html) |
| 08    | Funkcionální programování                                     | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/08/slides.html) |
| 09    | State & Visitor                                               | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/09/slides.html) |
| 10    | Procvičování design patternů                                  | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/10/slides.html) |
| 11    | Generické programování                                        | [ZDE](https://jarymiro.pages.fel.cvut.cz/omo2024/11/slides.html) |
