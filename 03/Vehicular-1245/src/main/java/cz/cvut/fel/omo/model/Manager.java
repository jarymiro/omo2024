package cz.cvut.fel.omo.model;

import java.util.Date;

public class Manager extends Employee {
    protected final Date promotionDate;
    public Manager(int id, String username, String forename, String surname, Address address, int employeeNo, Date employmentDate, Date promotionDate) {
        super(id, username, forename, surname, address, employeeNo, employmentDate);
        this.promotionDate = promotionDate;
    }
}
