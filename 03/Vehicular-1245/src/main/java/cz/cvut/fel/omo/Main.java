package cz.cvut.fel.omo;

import cz.cvut.fel.omo.model.Address;
import cz.cvut.fel.omo.model.Country;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Address address = new Address(Country.CZE, "Praha", "55501", "Technická", "2");
        System.out.println(address.toString());
    }
}