package cz.cvut.fel.omo.model;

public enum Country {
    CZE("Czech Republic", "CZK"),
    SVK("Slovak Republic", "SVK");

    Country(String name, String code) {
        this.name = name;
        this.code = code;
    }

    private String name;
    private String code;
}
