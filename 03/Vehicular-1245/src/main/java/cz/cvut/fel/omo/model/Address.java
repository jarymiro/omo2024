package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Address {
    private Country country;
    private String city;
    private String zip;
    private String street;
    private String streetNumber;
}
