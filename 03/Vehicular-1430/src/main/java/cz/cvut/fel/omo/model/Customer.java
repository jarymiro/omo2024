package cz.cvut.fel.omo.model;

import lombok.AccessLevel;
import lombok.Getter;

public class Customer extends Person {
    protected String driverLicenseNo;

    public Customer(String forename, String surname, Address address, String driverLicenseNo) {
        super(forename, surname, address);
        this.driverLicenseNo = driverLicenseNo;
    }
}
