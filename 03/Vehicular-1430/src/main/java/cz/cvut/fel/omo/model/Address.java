package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString(exclude = {"zip"})
public final class Address {
    private String city;
    private String street;
    private String houseNo;
    private Country country;
    private String zip;
}
