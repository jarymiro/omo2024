package cz.cvut.fel.omo.model;

import java.util.Date;

public class Manager extends Employee {
    protected final Date promotionDate;

    public Manager(String forename, String surname, Address address, int employeeNo, Date empolymentDate, Date promotionDate) {
        super(forename, surname, address, employeeNo, empolymentDate);
        this.promotionDate = promotionDate;
    }
}
