package cz.cvut.fel.omo.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
public abstract class Person {
    private static int count = 0;
    protected final int id;
    protected String forename;
    protected String surname;
    protected Address address;
    protected Date creationDate;

    public Person(int id, String forename, String surname, Address address, Date creationDate) {
        this.id = id;
        this.forename = forename;
        this.surname = surname;
        this.address = address;
        this.creationDate = creationDate;
    }

    public Person(String forename, String surname, Address address) {
        this(++count, forename, surname, address, new Date());
    }
}
