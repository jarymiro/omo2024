package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum VehicleType {
    CAR(0.5),
    VAN(0.6),
    TRUCK(0.6),
    BOAT(0.8),
    PLANE(1.2);

    private double margin;
}
