package cz.cvut.fel.omo.model;

import java.util.Set;

public final class Branch {
    private Manager manager;
    private Set<Employee> employees;
    private Set<Vehicle> vehicles;
    private Address address;
    private String name;
}
