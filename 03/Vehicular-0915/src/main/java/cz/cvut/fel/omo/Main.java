package cz.cvut.fel.omo;

import cz.cvut.fel.omo.model.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        Address address = new Address("Praha", "Tekvicová", "55", Country.CZE, "555");
        System.out.println(address.toString());
    }
}