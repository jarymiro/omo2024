package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;

import java.util.Date;

@AllArgsConstructor
public class Vehicle {
    private final VehicleType type;
    private String serialNo;
    private String registrationNo;
    private int seatCount;
    private Date dateOfManufacturing;
    private Date dateOfPurchase;
    private boolean isRented;
}
