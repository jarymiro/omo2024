package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;

import java.util.Date;

public class Employee extends Person {
    protected final Date dateOfEmployment;
    protected final int employeeNo;

    public Employee(int id, String forename, String surname, Address address, Date creationDate, Date dateOfEmployment, int employeeNo) {
        super(id, forename, surname, address, creationDate);
        this.dateOfEmployment = dateOfEmployment;
        this.employeeNo = employeeNo;
    }
}
