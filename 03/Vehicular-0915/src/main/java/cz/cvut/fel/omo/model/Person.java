package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
public abstract class Person {
    protected final int id;
    protected String forename;
    protected String surname;
    protected Address address;
    protected Date creationDate;
}
