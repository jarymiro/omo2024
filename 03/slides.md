---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 03

(Do)opakování Javy
Základní objektový návrh

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

# Dnešní téma

- další opakování Javy a OOP
  - vstup a výstup uživateli
  - rychlá vzpomínka na generika
  - kolekce & výjimky
  - práce s 3rd party knihovnami
- tvorba objektového návrhu
  - práce ve dvojicích
  - zamyšlení a nástřel tříd
  - vzájemné rozbíjení GITu 😭

![top-right width:650](../media/java-forgor-2.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Vstup a výstup uživateli

- třída `Scanner` pro snadné čtení vstupu
  - po řádcích, číslech...
  - jde o stream - po použití třeba uzavřít!
- `System.out` pro standardní výstup
  - určeno ke komunikaci s uživatelem aplikace
- `System.err` pro standardní chybový výstup
  - určeno pro výpis errorů, logování...

![top-right height:380](../media/smashing-computer.gif)

## Generika (Java Generics)

- možnost parametrizovat třídy
  - již od Javy 5 (2004)!
  - při instancování třídy předám
    jinou třídu jako "parametr"
  - nelze použít na primitivní typy!
  - původně vytvořeno pro kolekce
    (v `java.util.Collections`)
- obecně používáme když potřebujeme
  stejnou funkcionalitu pro více různých tříd
  - již zmíněné kolekce
  - matematické operace (objekt pro vektory - integer/double)
- dedikovaná přednáška ke konci semestru

![top-right width:680](../media/java-generics-meme.webp)

## Zápis generik v kódu

- definujeme v hlavičce třídy pomocí zástupného znaku
  - existují [jmenné konvence](https://docs.oracle.com/javase/tutorial/java/generics/types.html) na zástupné znaky

```java
public class Vector2D<N> {
    // místo konkrétní třídy používám zadefinovaný "placeholder"
    protected N x;
    protected N y;
    // u konstruktoru nepíšeme <>
    public Vector2D(N x, N y) {...}
    // Návratový typ bude opět zástupný znak
    public N getX() { return this.x }
    public N getY() { return this.y }
}

// Parametr třídy je doporučeno dávat pouze k typu proměnné. Ke konstruktoru ho Java dosadí sama.
Vector2D<Integer> integerVector = new 2DVector<>(2, 3);
Vector2D<Double> doubleVector = new 2DVector<>(5.8, 2.5);
integerVector.getX(); // Vrátí datový typ Integer o hodnotě 2
```

## Kolekce (Java Collections Framework)

- sada tříd pro práci s množinami objektů
  stejného typu
- implementována pomocí Java generik
- několik typů kolekcí
  - všechny dědí třídu `Collection`
  - každý typ má různé implementace

![center-right width:540](../media/java-collections-meme.webp)

## Přehled kolekcí

![bg height:75%](../media/java-collections-uml.svg)

## List<E>

- seznam prvků stejného typu `E`
- garantované pořadí prvků
- negarantovaná unikátnost prvků
- nejčastější implementace
  - `ArrayList` - "lepší pole"
  - `LinkedList` - spojový seznam
- **oproti poli nemá fixní délku!**

![center-right height:600](../media/java-array-meme.webp)

## Queue<E>

- fronta prvků stejného typu `E`
- garantované pořadí
- negarantovaná unikátnost prvků
- metody `add()`, `remove()` a `element()`
  vyhazují výjimky
- přidané metody bez výjimek
  `offer(e)`, `poll()`, `peek()`

![center-right height:650](../media/java-queue-meme.webp)

## Map<K, V>

- množina prvků klíč `K` -> hodnota `V`
- negarantované pořadí prvků
- garantována unikátnost **klíče**
  - avšak dva klíče mohou odkazovat
    na stejnou hodnotu
- nejčastější implementace
  - `HashMap` - hašovací tabulka
  - `TreeMap` - červenočerný strom, viz. další předměty

![center-right height:520](../media/java-map-meme.webp)

## Set<E>

- množina prvků stejného typu `E`
  - implementovaná pomocí mapy
- negarantované pořadí prvků
- garantovaná unikátnost prvků
  - kontrolováno pomocí metody `equals`
- nejčastější implementace
  - `HashSet` - hašovací tabulka
  - `TreeSet` - červenočerný strom, viz. další předměty

![center-right height:700](../media/java-set-meme.webp)

## Výjimky

- = události vyvolané v programu, které narušují jeho běžný chod
- alternativa pro ošetření chyb v programu
  - dělení nulou
  - chyba při čtení souboru
  - volání funkce/vlastnosti na objektu o hodnotě `null`
- výjimku lze zachytit pomocí try-catch bloku
  - nezachycená výjimka propadne do JVM,
    vypíše se její podrobnosti a program se ukončí

```
Exception in thread "main" java.lang.NullPointerException
    at NullPointerExample.main(NullPointerExample.java:4)
    at NullPointerExample.methodA(NullPointerExample.java:10)
    at NullPointerExample.methodB(NullPointerExample.java:15)
    at NullPointerExample.methodC(NullPointerExample.java:20)
```

## Typy výjimek

- checked exceptions
  - kontrola možnosti vyhození při kompilaci
  - můžeme se na ně připravit
  - `ParseException`, `IOException`...
- unchecked exceptions
  - nekontrolováno kompilátorem
  - potomci `RuntimeException`
  - `NullPointerException`, `ArrayIndexOutOfBoundsException`...

![top-right width:590](../media/java-exceptions-meme.webp)

## Vyhození výjimky

- pomocí klíčového slovíčka `throw`
  - výjimka musí implementovat rozhraní `Throwable`!
  - pokud to jde, používáme konkrétní výjimku
    (ne obyčejné `Exception`)
  - výjimky si můžeme vytvářet vlastní
    `class MyException extends Exception`

```java
/**
 * Pokud funkce vyhazuje checked výjimku, musíme tak uvést v deklaraci
 */
public double sqrt(double num) throws ArithmeticException {
  if (num < 0) {
    throw new ArithmeticException(
      "Square root of negative number is undefined."
    ); // Po throw se kód dále nevykonává
  }
  return Math.sqrt(num);
}
```

![bg right:35% width:500](../media/throw.webp)

## Zachycení výjimky

- obalením do tzv. try-catch bloku
  - do `try` vložíme funkce, které mohou výjimku vyhodit
  - v `catch` zpracujeme danou výjimku (dle potřeby)
  - bonus - `finally` blok se vykoná **vždy** po try bloku

```java
double num = -5.3; // Proběhne vždy
try {
  num = this.sqrt(num);
  // Zde mám již jistotu že výjimka nenastala
  System.out.println(num);
} catch (ArithmeticException e) {
   // Výjimka byla zachycena
   // zde proběhne její zpracování
  System.err.println(e.getMessage());
} finally {

}
```

![bg right:35% width:500](../media/catch.webp)

## Pokročilé zachycení výjimky

```java
double num = -5.3; // Proběhne vždy
try {
  double result = this.sqrt(num);
  // Zde mám již jistotu že výjimka nenastala
  System.out.println(result);
} catch (ArithmeticException e) {
   // Blok pro výjimky ArithmeticException a jejich potomky
   // Na pořadí catch bloků záleží - vykoná se first match!
   throw new MyCustomException("Vloženo nevalidní číslo!");
} catch (Exception e) {
  // Více generický blok pro jakékoliv výjimky
  System.err.println(e.getMessage());
} finally {
  // Blok se provede vždy (i pokud se výše uskuteční throw)
  logger.log(String.format("Called with num {}", num));
}
// Zde se kód provede pouze pokud nenastane první catch výše
```

![bg right:35% width:500](../media/catch-2.webp)

## Přidání externích knihoven do Mavenu
- najdu si knihovnu, co se mi líbí
  - ideálně v [oficiálním Maven repozitáři](https://mvnrepository.com/)
- vyberu si verzi (ideálně poslední stabilní)
- zkopíruju XML kód, který mi stránka nabídne
- vložím kód do XML elementu `<dependencies/>`

```xml
<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.18.32</version>
    <scope>provided</scope>
</dependency>
```

## Populární knihovny
- Quality of Life
  - [**Lombok**](https://projectlombok.org/) - automatické gettery, settery, 
    konstruktory...
  - [MapStruct](https://mapstruct.org/) - snadné mapování mezi objekty
- Testování kódu
  - [JUnit](https://junit.org) - jednotkové testy
  - [Mockito](https://site.mockito.org/) - "mockování" tříd
- Frameworky
  - [Spring](https://spring.io/) - snadná tvorba webových služeb
  - [Hibernate](https://hibernate.org/) - mapování objekt <-> databáze
- a další...
![top-right width:700](../media/java-library-meme.webp)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Základy objektového návrhu

- utvořit dvojice
- dále pokračovat na objektovém návrhu
- vyzkoušet si řešení konfliktů v GITu
