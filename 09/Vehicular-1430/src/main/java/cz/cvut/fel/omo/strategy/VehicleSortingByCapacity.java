package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleSortingByCapacity extends VehicleSorting {

    public VehicleSortingByCapacity(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = new ArrayList<>(branch.getVehicles());

        vehicles.sort((a, b) -> {
            if (a.getCapacity() > b.getCapacity()) {
                return 1;
            } else if (b.getCapacity() > a.getCapacity()) {
                return -1;
            } else {
                return 0;
            }
        });

        return vehicles;
    }


}
