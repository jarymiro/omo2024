package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleSortingByPrice extends VehicleSorting {

    public VehicleSortingByPrice(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = new ArrayList<>(branch.getVehicles());

        vehicles.sort(Vehicle.priceComparator);

        return vehicles;
    }


}
