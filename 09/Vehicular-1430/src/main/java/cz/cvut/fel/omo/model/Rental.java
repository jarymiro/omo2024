package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@ToString
public class Rental implements RentalFirstInterface, RentalSecondInterface {
    private Customer customer;
    private Vehicle vehicle;
    private List<Equipment> equipment;
    private Branch branch;
    private BigDecimal totalPrice;
    private Date rentedFrom;
    private Date rentedTo;

    /**
     * Checks if this rental is currently ongoing based on system date
     *
     * @return true if yes, false otherwise
     */
    public boolean isActive() {
        // TODO proper logic
        return true;
    }

    public BigDecimal calcTotalPrice() {
        return this.getTotalPrice();
    }
}
