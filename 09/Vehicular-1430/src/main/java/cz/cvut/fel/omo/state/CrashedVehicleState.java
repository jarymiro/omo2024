package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.Date;
import java.util.List;

public class CrashedVehicleState extends VehicleState {
    public CrashedVehicleState(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public Rental rent(Customer customer, List<Equipment> equipment, Date rentedFrom, Date rentedTo) {
        throw new IllegalStateException("Crashed vehicle cannot be rented!");
    }

    @Override
    public void unrent() {
        throw new IllegalStateException("Vehicle already returned!");
    }

    @Override
    public void repair() {
        // bankAccount - 1000;
        this.nextState = new InRepairVehicleState(vehicle);
    }

    @Override
    public void wash() {
        throw new IllegalStateException("Vehicle cannot be washed, fix it first!");
    }

    @Override
    public void crash() {
        this.nextState = this;
    }
}
