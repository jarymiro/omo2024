package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;
import org.apache.commons.lang3.NotImplementedException;

import java.util.Date;
import java.util.List;

public class RentedVehicleState extends VehicleState {
    public RentedVehicleState(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public Rental rent(Customer customer, List<Equipment> equipment, Date rentedFrom, Date rentedTo) {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void unrent() {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void repair() {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void wash() {
        throw new NotImplementedException("Do in your free time!");
    }

    @Override
    public void crash() {
        throw new NotImplementedException("Do in your free time!");
    }
}
