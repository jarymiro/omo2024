---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 09

State & Visitor

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

* diskuze ke 4. úkolu
* okénko k SP
* další designové vzory 🎉
  - State
  - Visitor 
* pokračování práce na společném kódu ![center-right width:600](../media/nice-codebase-meme.webp)
  - dopracování restů
  - spousta nových požadavků 😭

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Diskuze ke 4. úkolu

![bg height:70%](../media/hw4-meme2.webp)

## Okénko k SP
![center-right height:70%](../media/sp-free-hints.webp)

* reporty? jaké reporty? 
  - zprávy o běhu aplikace ("logy")
  - většinou ve formě txt souboru
  - kontextové informace - co se stalo, kdy apod...
* use-case diagramy dělat nemusíte
  - stačí textově zadefinovat
  - u předdefinovaných zadání většinou
    jde vytáhnout ze zadání
* jak vyřešit simulaci času?
  - ideálně pomocí "ticků" - diskrétní reprezentace 
* Java zdrojáky nahrávat nemusíte (když jsou na GL)
  - stačí nahrát jedno PDF (či více v ZIPu)

## [State](https://refactoring.guru/design-patterns/state) - když mají objekty složité stavy

* problém: objekt může mít vícero různých stavů,
  které se výrazně liší chováním ![top-right width:570](../media/uml-state.webp)
  - dokument může být označen jako rozpracovaný,
    publikovaný, archivovaný...
  - CD přehrávač může být spuštěný, pozastavený,
    otevřený, vypnutý...
* jistá podobnost s patternem Strategy
  - zde ale stavy o sobě navzájem vědí, a iniciují přechody

## [State](https://refactoring.guru/design-patterns/state) - když mají objekty složité stavy

CD přehrávač může být spuštěný, pozastavený...

```java
class CdPlayer {
  private PlayerState state;

  public void play() { this.state = state.play(); }
  public void pause() { this.state = state.pause(); }
}

class PausedState implements PlayerState {
  public PlayerState play() {
    // player logic
    return new PlayingState();
  }
  public PlayerState pause() {
    // already paused, nothing happens
    return this;
  }
}
```

![bg right:30% width:95%](../media/grandpa-cd-meme.webp)

## [State](https://refactoring.guru/design-patterns/state) - když mají objekty složité stavy

CD přehrávač může být spuštěný, pozastavený...

```java
class CdPlayer {
  private PlayerState state;

  public void play() { this.state = state.play(); }
  public void pause() { this.state = state.pause(); }
}

class PlayingState implements PlayerState {
  public PlayerState play() {
    // already playing, nothing happens
    return this;
  }
  public PlayerState pause() {
    // player logic
    return new PausedState();
  }
}
```

![bg right:30% width:95%](../media/grandpa-cd-meme.webp)

## [State](https://refactoring.guru/design-patterns/state) - když mají objekty složité stavy

verze s výčtovými typy

```java
class CdPlayer {
  private PlayerState state;

  public void play() { this.state = state.play(); }
  public void pause() { this.state = state.pause(); }
}

enum PlayerState {
  PLAY, PAUSE;
  public PlayerState play() {
    if (this == PLAY) { ... }
    else if (this == PAUSE) { ... } 
  }
  public PlayerState pause() {
    // heavy logic here
  }
}
```

![bg right:30% width:95%](../media/grandpa-cd-meme.webp)

## [State](https://refactoring.guru/design-patterns/state) - když mají objekty složité stavy

verze s výčtovými typy

```java
class CdPlayer {
  private PlayerState state;

  public void play() { this.state = state.play(); }
  public void pause() { this.state = state.pause(); }
}

enum PlayerState {
  PLAY, PAUSE;
  public PlayerState play() {
    if (this == PLAY) { ... }
    else if (this == PAUSE) { ... } 
  }
  public PlayerState pause() {
    // heavy logic here
  }
}
```
![bg right:30% width:95%](../media/grandpa-cd-meme.webp)
![bottom-center height:500](../media/red-cross.webp)


## [State](https://refactoring.guru/design-patterns/state) - když mají objekty složité stavy

verze s výčtovými typy

```java
enum PlayerState {
  PLAY {
    @Override
    public PlayerState play() {
      // already playing, nothing happens
      return this; // or return PLAY;
    }

    @Override
    public PlayerState pause() {
      // player logic
      return PAUSE;
    }
  }, PAUSE { ... };
  abstract public PlayerState play();
  abstract public PlayerState pause();
}
```

![bg right:30% width:95%](../media/grandpa-cd-meme.webp)

## [Visitor](https://refactoring.guru/design-patterns/visitor) - šikovná separace logiky

* problém: potřebujeme přidat třídám novou funkcionalitu
  - exportování datových tříd do souboru
  - mělo by být obsaženo v samotných datových třídách?
* řešení = Visitor ![bottom-right width:550](../media/uml-visitor.webp)
  - původním třídám přidáme metodu pro přijetí Visitora
  - každý Visitor má metodu na navštívení pův. třídy
* pro všímavé - jaký problém jsme právě vyřešili?
  * Double Dispatch!

## [Visitor](https://refactoring.guru/design-patterns/visitor) - šikovná separace logiky

export datové třídy do XML

```java
class BasicDataClass {
  private int id;
  private String name;
  private double multiplicator;
  
  public void accept(XmlExporter xmlExporter) {
    xmlExporter.exportBasic(this);
  }
}
class XmlExporter {
  public void exportBasic(BasicDataClass dataClass) {
      // Process BasicDataClass
  }
}

basicDataClass.accept(xmlExporter);
```
![bg right:33% height:98%](../media/visitor-exporter-meme.webp)

## [Visitor](https://refactoring.guru/design-patterns/visitor) - šikovná separace logiky

export datové třídy do XML

```java
class ExtraDataClass extends BasicDataClass {
  private String extraString;
  private int extraInt;

  @Override
  public void accept(XmlExporter xmlExporter) {
    xmlExporter.exportExtra(this);
  }
}
class XmlExporter {
  ...
  public void exportExtra(ExtraDataClass dataClass) {
    // Process ExtraDataClass
  }
}

extraDataClass.acceptExtra(xmlExporter);
```
![bg right:33% height:98%](../media/visitor-exporter-meme.webp)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - State a dopracování restů

* dopracovat resty z minulých cvičení
* vytvořit **State** pattern pro stav vozidla
  - vozidlo může být volné, zapůjčené, na vyčištění nebo na opravě
  - metody na vypůjčení, vrácení, vyčištění, opravu
* obohacení aplikace o ne-OMO věci
  - časová simulace
  - pomocné metody
  - TUI rozhraní