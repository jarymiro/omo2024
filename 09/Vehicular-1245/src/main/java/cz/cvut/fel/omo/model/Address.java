package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.*;

import java.util.Random;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Address {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private Country country;
    private String city;
    private String zip;
    private String street;
    private String streetNumber;

    public static Address generateRandom() {
        com.github.javafaker.Address fakeAddress = faker.address();
        return new Address(
                Country.getRandom(),
                fakeAddress.city(),
                fakeAddress.zipCode(),
                fakeAddress.streetAddress(),
                fakeAddress.buildingNumber()
        );
    }
}
