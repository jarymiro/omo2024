package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import cz.cvut.fel.omo.state.IdleState;
import cz.cvut.fel.omo.state.VehicleState;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Getter
@Setter
@ToString(exclude = "branch")
public class Vehicle {
    private static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    public static final Comparator<Vehicle> priceComparator = (a, b) -> {
        BigDecimal dailyPriceA = a.getDailyPrice();
        BigDecimal dailyPriceB = b.getDailyPrice();
        return dailyPriceA.compareTo(dailyPriceB);
    };

    private VehicleType vehicleType;
    private double mileage;
    private final Date productionDate;
    private String vinCode;
    private String spz;
    private String color;
    private String maker;
    private String model;
    private double tankCapacity;
    private Branch branch;
    private VehicleState state;

    public Vehicle(Date productionDate, VehicleType vehicleType, double mileage, String vinCode, String spz, String color, String maker, String model, double tankCapacity, Branch branch) {
        this.productionDate = productionDate;
        this.vehicleType = vehicleType;
        this.mileage = mileage;
        this.vinCode = vinCode;
        this.spz = spz;
        this.color = color;
        this.maker = maker;
        this.model = model;
        this.tankCapacity = tankCapacity;
        this.branch = branch;
        this.state = new IdleState(this);
    }

    public static Vehicle generateRandom() {
        return new Vehicle(
                faker.date().past(365 * 20, TimeUnit.DAYS),
                VehicleType.getRandom(),
                faker.random().nextDouble() * 20000,
                faker.idNumber().valid(),
                faker.idNumber().ssnValid(),
                faker.color().name(),
                faker.company().name(),
                faker.company().buzzword(),
                faker.random().nextDouble() * 100,
                null
        );
    }

    public Rental rentTo(Customer customer, List<Equipment> equipment, Date rentedFrom, Date rentedTo) {
        Rental rental = state.rentTo(customer, equipment, rentedFrom, rentedTo);
        this.state = state.getNextState();
        return rental;
    }

    public void unrent(Rental rental) {
        this.state.unrent(rental);
        this.state = state.getNextState();
    }

    public void wash() {
        this.state.wash();
        this.state = state.getNextState();
    }

    public void fix() {
        this.state.fix();
        this.state = state.getNextState();
    }

    public BigDecimal getDailyPrice() {
        return branch.getFuelPrice()
                .multiply(new BigDecimal(tankCapacity / 4))
                .multiply(BigDecimal.valueOf(vehicleType.getMargin()));
    }


}
