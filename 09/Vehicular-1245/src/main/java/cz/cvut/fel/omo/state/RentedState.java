package cz.cvut.fel.omo.state;

import cz.cvut.fel.omo.model.Customer;
import cz.cvut.fel.omo.model.Equipment;
import cz.cvut.fel.omo.model.Rental;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.Date;
import java.util.List;

public class RentedState extends VehicleState {
    public RentedState(Vehicle vehicle) {
        super(vehicle);
    }

    @Override
    public Rental rentTo(Customer customer, List<Equipment> equipment, Date rentedFrom, Date rentedTo) {
        throw new IllegalStateException("Vehicle is already rented.");
    }

    @Override
    public void unrent(Rental rental) {
        if (rental.isActive()) {
            this.nextState = new IdleState(this.vehicle);
        }
        throw new IllegalStateException("Provided rental is not active.");
    }

    @Override
    public void fix() {
        this.nextState = this;
    }

    @Override
    public void wash() {
        this.nextState = this;
    }
}
