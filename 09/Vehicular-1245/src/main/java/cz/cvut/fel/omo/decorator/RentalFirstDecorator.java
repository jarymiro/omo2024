package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalFirstInterface;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RentalFirstDecorator implements RentalFirstInterface {
    protected RentalFirstInterface wrappedRental;
}
