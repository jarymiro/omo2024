package cz.cvut.fel.omo.model;

import java.math.BigDecimal;

public interface RentalSecondLayer {
    public BigDecimal getPrice();
}
