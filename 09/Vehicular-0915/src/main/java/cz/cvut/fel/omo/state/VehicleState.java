package cz.cvut.fel.omo.state;

public abstract class VehicleState {
    public abstract VehicleState washVehicle();
    public abstract VehicleState repairVehicle();
    public abstract VehicleState returnVehicle();
    public abstract VehicleState rentVehicle();
}
