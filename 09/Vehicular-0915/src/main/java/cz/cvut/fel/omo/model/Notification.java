package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
public class Notification {
    private final String sender;
    private final String message;
    private final Date dateOfNotification;
}
