package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalSecondLayer;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RentalMultiplyDecorator implements RentalSecondLayer {
    protected RentalSecondLayer innerLayer;
}
