package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class VehicleSorting implements VehicleSortingInterface {
    protected Branch branch;

    public List<Vehicle> getSorted() {
        return branch.getVehicles().stream().toList();
    }
}
