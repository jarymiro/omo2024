# Vehicular - World-leading vehicle CRS software

CarRS is a Computer Reservation Software for managing and operating vehicle rental companies around the world. It handles information about branches, vehicle fleets and their availability in region. Thus the system serves the customers, employees and managers - everything is in one place.

## Functional requirements

1. The system holds records about all company branches; each branch has a manager, a set of employees and vehicles in its fleet.
2. All records are serializable - they can be stored/loaded from a file.
3. All record manipulation is logged - information about who changed what must be present. Record deletion has higher severity than other operations.
4. Employee can see the schedule & status of each vehicle. He can also mark it as unavailable for renting (due to its technical state etc.).
5. Manager of the branch has the same permissions in the system as its employee; moreover he can add/remove employees & vehicles from his branch, or modify their properties.
6. Customer can browse all company's branches, see vehicles available in it and is able to book specific car for specific date (for 1-30 whole days).
7. Each vehicle has it's daily rent cost, which is based on the fuel price (based on region), quarter of the fuel tank capacity and margin, which is shown in table below for each vehicle type.
8. Each vehicle has after each rent three-day cooldown in which it cannot be rented (so it can be washed, inspected etc.).
9. With each vehicle customer can rent some "equipment", which is not included in the base rent cost (caravan, hand truck, life jacket etc.). These items have predefined fixed daily cost (which already includes the margin for the company).

| Vehicle type | Margin | Notes |
| ------------ | ------ | ----- |
| Personal Car | 50%    |       |
| Van          | 60%    |       |
| Truck        | 60%    |       |
| Boat         | 80%    |       |
| Airplane     | 120%   |       |
