package cz.cvut.fel.omo.model;

public final class Address {
    private String city;
    private String street;
    private String houseNo;
    private Country country;
    private String zip;

    public Address(String city, String street, String houseNo, Country country, String zip) {
        this.city = city;
        this.street = street;
        this.houseNo = houseNo;
        this.country = country;
        this.zip = zip;
    }
}
