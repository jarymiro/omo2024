---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení 02

Opakování Javy
Základní objektový návrh

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

# Dnešní téma

- dotazy k 1. úkolu
- rychlé opakování Javy a OOP
  - vztahy mezi třídami, typy objektů
  - \+ nice to have věci
  - \+ lehké opakování UML
- tvorba objektového návrhu
  - práce ve dvojicích
  - zamyšlení a nástřel tříd
  - vzájemné rozbíjení GITu 😭

![top-right width:500](../media/java-forgor.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## 1. domácí úkol

Napište třídu Homework1, která má následující (neprivátní) metody:

- `boolean f()`, která vždy vrátí hodnotu true,
- statickou `boolean g()`, která vždy vrátí hodnotu false,
- `int h()`, která vždy vrátí počet volání metody h na této instanci (včetně právě probíhajícího volání, tzn. první volání metody h na dané instanci vrátí 1),
- `int i()`, která vždy vrátí počet volání metody i na libovolné instanci třídy Homework1 (včetně právě probíhajícího volání).

Problémy?

## Třídy a objekty

- třída = předpis vlastností a operací
- objekt = instance třídy
- konvence pro vytváření
  - třídy vytváříme vždy v samostatném, stejnojmenném souboru
  - názvy tříd zpravidla PascalCase,
    názvy vlastností a operací zpravidla camelCase
- novou instanci vytvoříme pomocí klíčového slova `new`
  - tím se zavolá tzv. konstruktor - metoda stejného jména jako třída
- každá třída má předdefinované metody
  - `equals()` pro ekvivalenci a `compareTo()` pro porovnání
  - a další...

## Zapouzdření

- (běžně) nechceme, aby stav třídy měnil kdokoliv
  - případně chceme změny nějak omezit (krajní hodnoty)
  - řešení = zapouzdření
- snažíme se být co nejvíce striktní

```java
class Counter {
  private int count;

  public int getCount() {
    return ++this.count;
  }

  public void resetCounter() {
    this.count = 0;
  }
}
```

![bg right:30% width:80%](../media/uml-class.webp)

## Úrovně viditelnosti

<!-- _class: lead -->

| Modifier      | Class | Package | Subclass | World |
| ------------- | ----- | ------- | -------- | ----- |
| public        | ✔️    | ✔️      | ✔️       | ✔️    |
| protected     | ✔️    | ✔️      | ✔️       | ❌    |
| _no modifier_ | ✔️    | ✔️      | ❌       | ❌    |
| private       | ✔️    | ❌      | ❌       | ❌    |

## Kompozice

- různé třídy mezi sebou mohou mít vazbu
  - dům se skládá z místností
  - počítač obsahuje CPU, GPU...
- kompozice = "has-a" vztah
  (jedna třída vlastní druhou)
- mnoho dalších druhů vazeb

```java
public class Room {
  private int area;
  private int doorCount;
}

public class House {
  private Room[] rooms;
}
```

![bg right:45% width:90%](../media/uml-composition.webp)

## UML vložka - typy vazeb

![bg height:70%](../media/uml-class-arrows.webp)

## Dědičnost

- různé třídy mohou mít podobné vlastnosti
  - **kočka** i **pes** jsou různá **zvířata**; mohou ale jíst, umřít, vydávat zvuky...
  - **auto** i **kamion** jsou různá **vozidla**; mají ale jiný počet míst, náprav...
- kopírovat kód nechceme; řešení = dědičnost
  - společné vlastnosti zabalím do nové, nadřazené třídy (rodič)
  - původním třídám (potomkům) sdělím, kdo je jejich rodič

```java
public class Animal {
  protected String name;
  public boolean eat(Food food) { ... }
}

public class Cat extends Animal { ... }
Cat myCat = new Cat();
myCat.eat(food);
```

![bottom-right width:590](../media/inheritance.webp)

## Vlastnosti dědičnosti

- potomci zdědí vlastnosti i metody rodiče
- potomci nedědí konstruktory s parametry!

Co potomek po předkovi zdědí zde? Dává kód smysl?

```java
public class Animal {
  protected String name;
  private int hp;

  public Animal(String name, int hp) { ... }
  protected boolean eat(Food food) { ... }
  private boolean heal(int newHp) { ... }
}

public class Dog extends Animal {
  public boolean givePaw() { ... }
}
```

![bg right:35% width:80%](../media/puppy-paw.gif)

## Abstraktní třída

- chci, aby někdo mohl instancoval třídu `Animal`?
  - asi úplně ne - v přírodě není žádné obecné zvíře
- řešení = abstraktní třída (klíčové slovo `extends`)
  - pouze jednoduchá dědičnost
  - pouhá deklarace metody pomocí `abstract`

```java
public abstract class Animal {
  protected int hp;

  abstract boolean eat(Food food);
  protected boolean heal(int newHp) { ... }
}

public class Dog extends Animal {
  public boolean givePaw() { ... }
}
```

![bg right:38% width:95%](../media/uml-abstract-class.webp)

## Interface

- chci mít všude stejné metody, ale jiné implementace
  - hodně advanced simulátor zvířat (žaludky, trávení...)
- řešení = interface
  - pouze deklarace proměnných a metod
    (ale existuje `default`)
  - proměnné jsou implicitně `public static final`
  - lze dědit od vícero interfaců; klíčové slovo `implements`

```java
public interface Growable {
  public void grow();
  public default void die() { ... }
}
public interface Eatable {
  public int getNutrition();
}
public class Melon implements Growable, Eatable { ... }
```

![bg right:32% width:95%](../media/uml-interface.webp)

## Record

- speciální typ immutable tříd
  - až od Javy 14 (2020!)
- kompilátor těmto typům automaticky
  - nastaví všechny proměnné jako `private final`
  - vytvoří veřejný konstruktor se všemi proměnnými
  - vygeneruje veřejné gettery všech proměnných
  - vytvoří metody `equals()`, `hashCode()` a `toString()`

```java
public record User(int id, String username) {
    public void someLogicMethod() {...}
}

User user = new User(1, "marnypetr");
System.out.println(user.username());
System.out.println(user.toString());
```

![top-right width:580](../media/java-record-meme.webp)

## Výčtové typy

- množina předdefinovaných konstant
  - také mohou mít vlastnosti a metody
  - názvy konstant zpravidla velkými písmeny

```java
public enum Color { BLACK, BLUE, RED, YELLOW }
Color selectedColor = Color.BLACK;
/* ------------------------------------------ */
public enum FuelType {
  GASOLINE(38.2), DIESEL(38.0), LPG(16.7);

  private float price;
  FuelType(float price) { ... }

  public float getPrice() { return this.price }
}
FuelType myFuel = FuelType.LPG;
myFuel.getPrice(); // 16.7
```

![bg right:45% width:95%](../media/java-enum-meme.webp)

## Přetížení metod (overloading)

- více variant stejnojmenné metody
  s jinými parametry
  - konstruktory s různými parametry
  - mat. funkce pro různé číselné typy

```java
public class Math {
  public static long sum(short... nums) { ... }

  public static long sum(int... nums) { ... }

  public static long sum(long... nums) { ... }

  public static double sum(float... nums) { ... }

  public static double sum(double... nums) { ... }
}
```

![bg width:95% right:40%](../media/java-overload.webp)

## Přepisování metod (overriding)

- potomek přepisuje metodu rodiče
  - stejný název, stejné parametry
  - nepovinná, ale doporučená anotace `@Override`
    (abychom omylem neoverloadovali)

```java
public class Dog extends Animal {
  public boolean givePaw() { ... }
}

public class Wolf extends Dog {
  @Override
  public boolean givePaw() {
    this.attack();
  }
}
```

![bg height:95% right:35%](../media/java-override.webp)

## Modifikátor final

- u proměnné - hodnotu nelze změnit po inicializaci
- u metody - nelze přetížit metodou potomka
- u třídy - nemůže mít potomka

```java
public final class Cat {
  private static int catCounter = 0;
  private final int id;

  public final Cat() {
    this.id = catCounter++;
  }

  public final int getId() {
    return this.id;
  }
}
```

![bg right:35% width:100%](../media/final.webp)

## Rychlá připomínka - nahrávejte práci na GIT!

- vše dávejte do předem vytvořeného repozitáře
  [gitlab.fel.cvut.cz/B241_B6B36OMO/username](https://gitlab.fel.cvut.cz/B241_B6B36OMO/username)
- na konci každého cvičení nezapomeňte
  práci pushnout
- semestrálku do podsložky `sem`
  (jen jeden z vás 😉)

![top-right width:550](../media/git_fire.webp)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Základy objektového návrhu

- utvořit dvojice
- pročíst si zadání CRS
- utvořit (alespoň částečně) objektový návrh
- vyzkoušet si řešení konfliktů v GITu
