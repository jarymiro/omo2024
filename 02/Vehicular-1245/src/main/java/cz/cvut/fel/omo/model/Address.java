package cz.cvut.fel.omo.model;

public class Address {
    private Country country;
    private String city;
    private String zip;
    private String street;
    private String streetNumber;

    public Address(Country country, String city, String zip, String street, String streetNumber) {
        this.country = country;
        this.city = city;
        this.zip = zip;
        this.street = street;
        this.streetNumber = streetNumber;
    }
}
