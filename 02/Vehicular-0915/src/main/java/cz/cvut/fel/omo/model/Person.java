package cz.cvut.fel.omo.model;

import java.util.Date;

public abstract class Person {
    protected final int id;
    protected String forename;
    protected String surname;
    protected Address address;
    protected Date creationDate;
}
