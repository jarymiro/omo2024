package cz.cvut.fel.omo.model;

public enum Country {
    CZE("Czech Republic", "CZE");

    private final String name;
    private final String code;

    Country(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
