---
marp: true
theme: cvut
size: 16:9
footer: "Bc. Miroslav Jarý <[jarymiro@cvut.cz](mailto:jarymiro@cvut.cz)>"
header: "B6B36OMO - Objektový návrh a modelování"
paginate: true
headingDivider: 2
---

<!-- _class: lead gaia -->
<!-- _paginate: false -->

# Cvičení ~~04~~ 08

Funkcionální programování

[PDF ke stažení](slides.pdf)
![bg right:40% height:500](qrcode.webp)

## Dnešní téma

* diskuze k 3. úkolu
* funkcionální programování v Javě
  - lambda výrazy
  - supplier & consumer
  - procedury vs funkce
  - Stream API
* zadání 4. úkolu

![center-right width:700](../media/fup-meme.webp)

## Ale nejdříve...

![bg height:90%](../media/kahoot.webp)

## Diskuze k 3. úkolu

![bg height:70%](../media/hw3-meme2.webp)

## Opakování FuP z přednášky

* rozdíl mezi mutable/immutable?
![center-right width:520](../media/immutable-meme.webp)
  * lze/nelze změnit stav či vlastnosti daného objektu
* funkce prvního řadu?
  * může být předána jako parametr, přiřazena proměnné
    a být vrácena z funkce
* funkce vyššího řádu?
  * jedním či více parametrů je funkce
    nebo funkci vrací

## Opakování FuP z přednášky


* currying?
  ![center-right width:650](../media/fup-flashback-meme.webp)
  - vyhodnocení funkce "per partes"
  - 3 + 3 + 3 => 6 + 3 => 9
* pure funkce?
  - funkce nemění a není měněna
    vnějším stavem tříd/objektů
* lazy evaluace?
  - vyhodnocení až ve chvíli, kdy si o to řeknu
  - příkladem jsou třeba lambda výrazy

## Lambda výrazy

- od Javy 8
- převzato z funkcionálního programování
  - `parametr -> výraz`
  - `parametr -> {blok kódu}`

```java
List<String> names = new LinkedList<>();
names.add("Petr");
names.add("Ondřej");
names.add("Xaver");

// Zápis for-each loopu objektově
for (String name : names) {
    System.out.println(name);
}

// Zápis for-each loopu funkcionálně
names.forEach(name -> System.out.println(name));
```

![top-right width:170](../media/lambda.gif)

## Lambda výrazy

- parametrů může být i více
  - zabalíme je do závorek
  - méně časté, ale stále používané

```java
Map<String, BigDecimal> salary = new HashMap<>();
salary.put("Petr", new BigDecimal(15000));
salary.put("Ondřej", new BigDecimal(50000));
salary.put("Xaver", new BigDecimal(35000));

// Zápis for-each loopu objektově
for (Entry<String, BigDecimal> entry : salary.entrySet()) {
    System.out.println(String.format("%s: %.2f Kč", entry.getKey(), entry.getValue()));
}

// Zápis for-each loopu funkcionálně
salary.forEach((name, value) -> {
    System.out.println(String.format("%s: %.2f Kč", name, value))
});
```

![top-right width:170](../media/lambda.gif)

## Lambda výrazy - \*Supplier

- výrazy vytvářející nové objekty dané třídy
- lze ukládat do proměnné
  - `Supplier<T>`, kde `T` je instancovaná třída
- metoda `get()` určena pro vytvoření nového objektu

```java
abstract class Food {}
class Steak extends Food {}
class CannedFood extends Food {}

Supplier<Food> foodSuplierA = () -> new Steak();
Supplier<Food> foodSuplierB = () -> new CannedFood();

Food catFood = foodSuplierA.get();
Food dogFood = foodSuplierB.get();
```

![bg right:35% width:95%](../media/producer.webp)

## Lambda výrazy - \*Consumer

- zpracovávají objekty dané třídy (a.k.a. procedury)
- lze ukládat do proměnné
  - `Consumer<T>` pro proceduru s jedním parametrem
  - `BiConsumer<T, U>` pro proceduru s dvěma parametry
- metodou `accept()` vykonáme danou proceduru

```java
Consumer<Food> simpleFoodConsumer = (food) -> {
  System.out.printf("Consumed %s", food);
}
BiConsumer<Food, Food> complexFoodConsumer = (foodA, foodB) -> {
  System.out.printf("Consumed %s with %s. Yum yum!", foodA, foodB);
}

Food catFood = foodSuplierA.get();
Food dogFood = foodSuplierB.get();
simpleFoodConsumer.accept(catFood);
complexFoodConsumer.accept(catFood, dogFood);
```

![bg right:35% width:95%](../media/consumer.gif)

## Lambda výrazy - \*Function

- třídy určené pro ukládání funkcí do proměnných
  - `Function<T, R>` pro funkci s jedním parametrem
  - `BiFunction<T, U, R>` pro funkci s dvěma parametry
- metodou `apply()` vykonáme danou funkci

```java
BiFunction<Food, Food, Double> calcCalories = (foodA, foodB) -> {
    return 1.5*foodA.getCalories() + 1.25*foodB.getCalories();
};

Food catFood = foodSuplierA.get();
Food dogFood = foodSuplierB.get();

Double calories = calcCalories.apply(catFood, dogFood);
System.out.printf("%.1f kcal\n", calories);
```

![bg right:35% width:95%](../media/homer-huh.gif)

## Stream API
- koncept obdobný z IO streamů
  - práce s daty nikoliv jako s jednotlivými objekty, ale proudem
- používá se zde
  - design pattern [Builder](https://refactoring.guru/design-patterns/builder)
  - ve velkém lambda výrazy
- stream mohu vytvořit z libovolné jednotypové kolekce
  - následně mohu nějak data modifikovat
  - výsledek streamu může být nový objekt nebo nová kolekce (kumulace)
```java
List<String> carmakers = Arrays.asList("Audi", "BMW", "Skoda", "VW");

List<String> result = carmakers.stream() // Vytvořím stream
    .filter(carmake -> carmake.length() < 3) // lambda výraz vracející true/false
    .collect(Collectors.toList()); // Uložím data do nové kolekce
```

![top-right width:400](../media/stream.gif)

## Stream API - časté metody
- data můžeme modifikovat několika způsoby
  - `reduce()` - spojuje data "do sebe" (suma, spojování stringů)
  - `filter()` - odstraňuje data ze streamu na základě podmínky
  - `map()` - mapuji z původní hodnoty na novou (funkce)
  - `sort()` - řadím data dle nějaké logiky
  - `distinct()` - zahodí duplicitní hodnoty
  - ...
- výsledek dat můžeme vytvořit několika způsoby
  - `collect()` - zbytek dat ve streamu převede do nové kolekce
  - `count()` - spočte počet prvků ve streamu
  - ...

## Stream API vs for cykly
- úkol: sečti všechna lichá čísla menší než 10
```java
List<Integer> data = Arrays.asList(2, 3, 4, 8, 7, 5, 8, 154, -5);

// Klasický přístup
int classicSum = 0;
for (int num : data) {
    if (num < 10 && num % 2 == 1) {
        classicSum += num;
    }
}
System.out.println(classicSum);

// Pomocí Stream API
int streamSum = data.stream()
        .filter(num -> num < 10 && num % 2 == 1)
        .reduce(0, (a, b) -> a + b);
System.out.println(streamSum);
```

## Stream API vs for cykly
- úkol: seřaď abecedně jména lidí, kterým je méně něž 30 let
```java
Map<String, Integer> people = Map.of("Petr", 28, "Lucka", 35, "Jan", 21);

// Klasický přístup
List<String> classicResult = new ArrayList<>();
for (Map.Entry<String, Integer> person : people.entrySet()) {
    if (person.getValue() < 30) {
        classicResult.add(person.getKey());
    }
    // Následně udělat bubblesort či jiný algoritmus...
}

// Pomocí Stream API
List<String> streamResult = people.entrySet().stream()
        .filter(person -> person.getValue() >= 30)
        .map(data -> data.getKey())
        .sorted()
        .collect(Collectors.toList());
```

## [4. domácí úkol](https://cw.fel.cvut.cz/b241/courses/b6b36omo/hw/042023/start)

* deadline 01. 12.
* implementace metod
  - s komentářem `TODO Homework`
  * pouze za použití Stream API ![center-right width:750](../media/hw4-meme.webp)
* využijte připravenou [šablonu](https://cw.fel.cvut.cz/b241/_media/courses/b6b36omo/omo-hw3-students_1.zip)

## Pojďme do práce!

<!-- _class: lead gaia center -->

#### Dnešní úkol - Decorator a Stream API

* vytvořit/dokončit pokročilý **Decorator**
  - **RentalMultiplicationDecorator** - **RentalExtraFuel** a **RentalPersonalDriver**
  - **RentalAdditionDecorator** - **RentalExtraPassenger**
  - nejdřív sčítáme, pak násobíme
* vyzkoušet si práci se Stream API
  - získej všechna auta levnější než **x** seřazená podle ceny
  - získej všechna auta pro více než **x** lidí seřazená podle kapacity