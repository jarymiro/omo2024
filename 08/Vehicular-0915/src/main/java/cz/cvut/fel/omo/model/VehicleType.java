package cz.cvut.fel.omo.model;

import cz.cvut.fel.omo.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Random;

@AllArgsConstructor
@Getter
public enum VehicleType {
    CAR(0.5),
    VAN(0.6),
    TRUCK(0.6),
    BOAT(0.8),
    PLANE(1.2);

    private static final Random random = new Random(Constants.RANDOM_SEED);

    private double margin;

    public static VehicleType getRandom() {
        return values()[random.nextInt(values().length)];
    }
}
