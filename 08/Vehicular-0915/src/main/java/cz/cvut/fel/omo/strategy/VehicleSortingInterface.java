package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Vehicle;

import java.util.List;

public interface VehicleSortingInterface {
    public List<Vehicle> getSorted();
}
