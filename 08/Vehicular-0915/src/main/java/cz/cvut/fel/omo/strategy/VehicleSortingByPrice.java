package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.math.BigDecimal;
import java.util.List;

public class VehicleSortingByPrice extends VehicleSorting {
    public VehicleSortingByPrice(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = branch.getVehicles()
                .stream()
                .sorted(Vehicle.priceComparator)
                .toList();

        return vehicles;
    }
}
