package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;

import java.util.concurrent.TimeUnit;

public class FakeCustomerAdapter extends Customer {
    private static final Faker faker = new Faker();

    public FakeCustomerAdapter() {
        super(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS),
                faker.idNumber().valid()
        );
    }
}
