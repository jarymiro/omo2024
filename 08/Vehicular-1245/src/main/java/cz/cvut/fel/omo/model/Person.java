package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Random;

@Getter
@Setter
@ToString
public abstract class Person {
    protected static final Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    protected String username;
    protected String forename;
    protected String surname;
    protected Address address;
    protected final Date creationDate;

    public Person(String username, String forename, String surname, Address address) {
        this(username, forename, surname, address, new Date());
    }

    public Person(String username, String forename, String surname, Address address, Date creationDate) {
        this.username = username;
        this.forename = forename;
        this.surname = surname;
        this.address = address;
        this.creationDate = creationDate;

    }
}
