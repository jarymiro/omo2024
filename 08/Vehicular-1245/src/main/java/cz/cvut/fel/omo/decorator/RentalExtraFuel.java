package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalSecondInterface;

import java.math.BigDecimal;

public class RentalExtraFuel extends RentalSecondDecorator {
    public RentalExtraFuel(RentalSecondInterface wrappedRental) {
        super(wrappedRental);
    }

    @Override
    public BigDecimal getCalculatedTotalPrice() {
        return wrappedRental.getCalculatedTotalPrice()
                .multiply(new BigDecimal(2));
    }
}
