package cz.cvut.fel.omo.strategy;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

import java.util.List;

public class VehicleSortingByMileage extends VehicleSorting {
    public VehicleSortingByMileage(Branch branch) {
        super(branch);
    }

    public List<Vehicle> getSortedVehicles() {
        return branch.getVehicles().stream()
                .sorted((a, b) -> {
                    if (a.getMileage() > b.getMileage()) {
                        return -1;
                    } else if (b.getMileage() > a.getMileage()) {
                        return 1;
                    } else {
                        return 0;
                    }
                }).toList();
    }
}
