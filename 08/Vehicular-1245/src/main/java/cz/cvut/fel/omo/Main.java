package cz.cvut.fel.omo;

import cz.cvut.fel.omo.decorator.RentalExtraFuel;
import cz.cvut.fel.omo.factory.VehicleFactory;
import cz.cvut.fel.omo.legacymodel.LegacyCzechAddress;
import cz.cvut.fel.omo.legacymodel.LegacyCzechAddressAdapter;
import cz.cvut.fel.omo.legacymodel.LegacyRental;
import cz.cvut.fel.omo.legacymodel.LegacyRentalAdapter;
import cz.cvut.fel.omo.model.*;
import cz.cvut.fel.omo.strategy.VehicleSorting;
import cz.cvut.fel.omo.strategy.VehicleSortingByMileage;
import cz.cvut.fel.omo.strategy.VehicleSortingByPrice;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        System.out.println(Address.generateRandom());
        System.out.println(Customer.generateRandom());
        System.out.println(Employee.generateRandom());
        System.out.println(Manager.generateRandom());
        System.out.println(Vehicle.generateRandom());

        Date fromDate = Date.from(Instant.now().minusSeconds(3600));
        Date toDate = Date.from(Instant.now().plusSeconds(3600));
        Rental rental = new Rental(null, null, null, null, null, fromDate, toDate);
        System.out.println(rental.isActive());


        LegacyRental legacyRental = new LegacyRental(
                "pepa123",
                "1A5 2225",
                300,
                5
        );
        LegacyRentalAdapter adapted = new LegacyRentalAdapter(legacyRental);
        System.out.println(adapted.getTotalPrice());

        LegacyCzechAddress legacyCzechAddress = new LegacyCzechAddress(1603, 2, "Praha", "Technická");
        LegacyCzechAddressAdapter legacyCzechAddressAdapter = new LegacyCzechAddressAdapter(legacyCzechAddress);
        System.out.println(legacyCzechAddressAdapter);

        Branch newYork = new Branch(legacyCzechAddressAdapter, null, new BigDecimal(15));
        Customer pepa = Customer.generateRandom();
        Customer pepaB = Customer.generateRandom();
        Customer pepaC = Customer.generateRandom();
        Customer pepaD = Customer.generateRandom();

        newYork.addSubscriber(pepa);
        newYork.addSubscriber(pepaB);

        Vehicle ford = new Vehicle(new Date(), VehicleType.PERSONALCAR, 0, "AAA BBB", "5A1 2255", "Wine", "Ford", "Galaxy", 50, null);
        newYork.addVehicleToFleet(ford);

        newYork.removeSubscriber(pepaB);
        VehicleFactory factory = new VehicleFactory(null);
        Vehicle truck = factory.makeNewVolvoTruck("DDD 555", "5A3 6547", "Blue", "FH16", 300);
        newYork.addVehicleToFleet(truck);
        newYork.addVehicleToFleet(Vehicle.generateRandom());
        newYork.addVehicleToFleet(Vehicle.generateRandom());
        newYork.addVehicleToFleet(Vehicle.generateRandom());
        newYork.addVehicleToFleet(factory.createFakeVehicle());

        VehicleSorting sorting = new VehicleSortingByMileage(newYork);
        System.out.println(sorting.getSortedVehicles());
        sorting = new VehicleSortingByPrice(newYork);
        System.out.println(sorting.getSortedVehicles());

        // RentalInterface rental = new RentalExtraFuel(
        //         new RentalExtraFuel(
        //                 new RentalExtraFuel(
        //                         new Rental()
        //                 )
        //         )
        // );
        System.out.printf("Branch %s worth: %.2f\n", newYork.getAddress(), newYork.getBranchNetWorth());

    }


}