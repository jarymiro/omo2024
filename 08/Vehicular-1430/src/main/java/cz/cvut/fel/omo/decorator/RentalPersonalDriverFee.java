package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalSecondInterface;

import java.math.BigDecimal;

public class RentalPersonalDriverFee extends RentalSecondDecorator {
    public RentalPersonalDriverFee(RentalSecondInterface wrappedRental) {
        super(wrappedRental);
    }

    @Override
    public BigDecimal calcTotalPrice() {
        return wrappedRental.calcTotalPrice().multiply(new BigDecimal(1.5));
    }
}
