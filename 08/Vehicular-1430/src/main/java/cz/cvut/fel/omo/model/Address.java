package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;
import cz.cvut.fel.omo.Constants;
import lombok.*;

import java.util.Random;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Address {
    private static Faker faker = new Faker(new Random(Constants.RANDOM_SEED));

    private String city;
    private String street;
    private String houseNo;
    private Country country;
    private String zip;

    public static Address generateRandom() {
        com.github.javafaker.Address randomAddress = faker.address();
        String city = randomAddress.city();
        String street = randomAddress.streetAddress();
        String houseNo = randomAddress.buildingNumber();
        Country country = Country.CZECHIA;
        String zip = randomAddress.zipCode();

        return new Address(city, street, houseNo, country, zip);
    }
}
