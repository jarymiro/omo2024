package cz.cvut.fel.omo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@ToString(exclude = {"vehicles", "employees", "subscribedCustomers"})
public class Branch {
    private BigDecimal fuelPrice;
    private Address address;
    private String name;
    private List<Employee> employees;
    private Manager manager;
    private List<Vehicle> vehicles;
    private List<Customer> subscribedCustomers;

    public Branch(BigDecimal fuelPrice, Address address, String name, Manager manager) {
        this(fuelPrice, address, name, new ArrayList<>(), manager, new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Employ provided person in this branch
     *
     * @param person
     */
    public void employPerson(Person person) {
        // TODO proper logic
    }

    /**
     * Promote provided employee to a manager of the branch.
     *
     * @param employee
     */
    public void promoteToManager(Employee employee) {
        // TODO proper logic
    }

    /**
     * Add provided vehicle to the branch's fleet
     *
     * @param vehicle
     */
    public void addVehicleToFleet(Vehicle vehicle) {
        if (vehicle.getBranch() == null && !vehicles.contains(vehicle)) {
            vehicle.setBranch(this);
            vehicles.add(vehicle);
            Message message = new Message(this.name, String.format("BIG NEWS!!!! We added vehicle %s to branch %s. Come to our branch and pick it up!!!!", vehicle, this.name));
            for (Customer customer : subscribedCustomers) {
                customer.recieve(message);
            }
        } else {
            System.err.println("vehicle in branch already");
        }

    }

    public void addSubscriber(Customer subscriber) {
        subscribedCustomers.add(subscriber);
    }

    public void removeSubscriber(Customer subscriber) {
        subscribedCustomers.remove(subscriber);
    }

    public List<Vehicle> getVehiclesCheaperThan(double maxValue) {
        return this.getVehicles().stream()
                .filter(vehicle -> vehicle.getDailyPrice().doubleValue() < maxValue)
                .sorted(Vehicle.priceComparator)
                .toList();
    }

    public BigDecimal calculateBranchValue() {
        return this.getVehicles().stream()
                .map(vehicle -> vehicle.getDailyPrice())
                .reduce(new BigDecimal(0), (sum, price) -> sum.add(price));

    }
}
