package cz.cvut.fel.omo.model;

import java.math.BigDecimal;

public interface RentalFirstInterface {
    public BigDecimal calcTotalPrice();
}
