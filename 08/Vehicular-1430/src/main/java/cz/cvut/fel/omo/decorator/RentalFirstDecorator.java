package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalFirstInterface;
import cz.cvut.fel.omo.model.RentalSecondInterface;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RentalFirstDecorator implements RentalFirstInterface, RentalSecondInterface {
    public RentalFirstInterface wrappedRental;
}
