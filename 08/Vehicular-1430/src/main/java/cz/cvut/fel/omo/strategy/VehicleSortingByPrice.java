package cz.cvut.fel.omo.strategy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cz.cvut.fel.omo.model.Branch;
import cz.cvut.fel.omo.model.Vehicle;

public class VehicleSortingByPrice extends VehicleSorting {

    public VehicleSortingByPrice(Branch branch) {
        super(branch);
    }

    @Override
    public List<Vehicle> getSorted() {
        List<Vehicle> vehicles = new ArrayList<>(branch.getVehicles());
        
        vehicles.sort(Vehicle.priceComparator);

        return vehicles;
    }

    
}
