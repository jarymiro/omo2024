package cz.cvut.fel.omo.model;

import com.github.javafaker.Faker;

import java.util.concurrent.TimeUnit;


public class FakeEmployeeAdapter extends Employee {
    private static final Faker faker = new Faker();

    public FakeEmployeeAdapter() {
        super(
                faker.name().firstName(),
                faker.name().lastName(),
                Address.generateRandom(),
                faker.date().past(365, TimeUnit.DAYS)
        );
    }
}
