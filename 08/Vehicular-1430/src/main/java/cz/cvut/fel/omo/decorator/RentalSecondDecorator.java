package cz.cvut.fel.omo.decorator;

import cz.cvut.fel.omo.model.RentalSecondInterface;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RentalSecondDecorator implements RentalSecondInterface {
    public RentalSecondInterface wrappedRental;
}
